;;; erc-tests-compat.el --- Backports for ERC tests -*- lexical-binding: t -*-

;;; Commentary:
;;; Code:

(if (> emacs-major-version 28)
    (provide 'erc-tests-compat)
  (require 'erc-tests-28-compat))

(if (> emacs-major-version 27)
    (provide 'erc-tests-compat)
  (require 'erc-tests-27-compat))

(provide 'erc-tests-compat)

;;; erc-tests-compat.el ends here
