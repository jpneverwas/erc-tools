;;; erc-tests-27-compat.el --- Emacs 27 backports -*- lexical-binding: t -*-

;;; Commentary:

;; Functions stolen from Emacs 28 and 29 for running ERC tests in 27

;;; Code:

(require 'cl-macs)
(require 'subr-x)

;;;; lisp/emacs-lisp/lisp-mode.el
(defvar lisp-data-mode-syntax-table lisp--mode-syntax-table)

;;;; lisp/emacs-lisp/ert-x.el

(defvar ert-resource-directory-format "%s-resources/"
  "Format for `ert-resource-directory'.")
(defvar ert-resource-directory-trim-left-regexp ""
  "Regexp for `string-trim' (left) used by `ert-resource-directory'.")
(defvar ert-resource-directory-trim-right-regexp "\\(-tests?\\)?\\.el"
  "Regexp for `string-trim' (right) used by `ert-resource-directory'.")

(declare-function macroexp-file-name "macroexp" nil)

;; Has to be a macro for `load-file-name'.
(defmacro ert-resource-directory ()
  "Return absolute file name of the resource (test data) directory.

The path to the resource directory is the \"resources\" directory
in the same directory as the test file this is called from.

If that directory doesn't exist, find a directory based on the
test file name.  If the file is named \"foo-tests.el\", return
the absolute file name for \"foo-resources\".  If you want a
different resource directory naming scheme, set the variable
`ert-resource-directory-format'.  Before formatting, the file
name will be trimmed using `string-trim' with arguments
`ert-resource-directory-trim-left-regexp' and
`ert-resource-directory-trim-right-regexp'."
  `(let* ((testfile ,(or (macroexp-file-name)
                         buffer-file-name))
          (default-directory (file-name-directory testfile)))
     (file-truename
      (if (file-accessible-directory-p "resources/")
          (expand-file-name "resources/")
        (expand-file-name
         (format ert-resource-directory-format
                 (string-trim testfile
                              ert-resource-directory-trim-left-regexp
                              ert-resource-directory-trim-right-regexp)))))))

(defmacro ert-resource-file (file)
  "Return file name of resource file named FILE.
A resource file is in the resource directory as per
`ert-resource-directory'."
  `(expand-file-name ,file (ert-resource-directory)))

(defmacro ert-simulate-keys (keys &rest body)
  "Execute BODY with KEYS as pseudo-interactive input."
  (declare (debug t) (indent 1))
  `(let ((unread-command-events
          ;; Add some C-g to try and make sure we still exit
          ;; in case something goes wrong.
          (append ,keys '(?\C-g ?\C-g ?\C-g)))
         ;; Tell `read-from-minibuffer' not to read from stdin when in
         ;; batch mode.
         (executing-kbd-macro t))
     ,@body))

;;;; lisp/emacs-lisp/lisp-mode.el

(defalias 'lisp-data-mode #'lisp-mode)

(provide 'erc-tests-27-compat)
;;; erc-tests-27-compat.el ends here
