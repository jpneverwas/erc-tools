;;; erc-can.el --- Ugly helpers for canning logs -*- lexical-binding: t; -*-

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The command `erc-can-convert-protocol-log' asks for a file containing the
;; contents of an *erc-protocol* buffer (see `erc-log-irc-protocol').  It'll
;; attempt to convert this into a "canned dialog" of Lisp-Data readable by the
;; `erc-d' server.  Upon completion, canned logs should be inspected and
;; touched up manually.

;; TODO
;;
;; - Rewrite "redact" nonsense to operate on original log (not structured Lisp
;;   Data).  See older commits around 53d2d13ecf5c08c09bd039c6cbdfdfe217be6b42
;;   for half-baked redaction facility.

;;; Code:

(require 'erc-d-i)
(require 'cl-extra)
(require 'parse-time)

(defvar erc-can--protocol-regexp-in
  (rx bol
      (group (+ (any digit "T:.+-"))) " " (group (+ (not " ")))
      " >> "
      (group (? "@" (+ (not " ")) " ")
             (group (+ (any upper digit)))
             (? " " (+ (not (any "\r" "\n")))))
      (* (any "\r" "\n"))
      eol))

(defvar erc-can--protocol-regexp-out
  (rx bol
      (group (+ (any digit "T:.+-"))) " " (group (+ (not " ")))
      " << "
      (group (+ (not (any "\r" "\n"))))
      (* (any "\r" "\n"))
      eol))

(defvar erc-can--ignored-requests '("PING"))
(defvar erc-can--ignored-replies '("PONG"))

(defvar erc-can--float-format-out nil)
(defvar erc-can--float-max-out nil)

;; Can't really use cl-prettyprint or similar because they need to use
;; lisp-ppss, etc. to inspect the shape of the data (which we already know).
(defun erc-can--ugly-print (hunk)
  "Print HUNK."
  (let ((standard-output (current-buffer))
        (print-escape-control-characters t)
        (float-format (and erc-can--float-format-out
                           `((float-format . ,erc-can--float-format-out)))))
    (insert "(")
    (prin1 (pop hunk))
    (while hunk
      (insert "\n ")
      (prin1 (pop hunk) nil float-format))
    (insert ")\n")))

(defun erc-can--convert-setup-output-buffer (name)
  "Set up output buffer using NAME, creating it if necessary."
  (setq name (replace-regexp-in-string "/" "-" name))
  (let ((outbuf (get-buffer-create (format "*erc-can-read-%s*" name))))
    (with-current-buffer outbuf
      (unless (not (eq major-mode 'fundamental-mode))
        (lisp-data-mode))
      (add-file-local-variable-prop-line 'mode 'lisp-data)
      (setq truncate-lines t))
    outbuf))

(defun erc-can--diff-time (old new)
  "Subtract OLD from NEW."
  (cl-assert (or (time-less-p old new) (time-equal-p old new)))
  (/ (fround (* (float-time (time-subtract new old)) 100)) 100))

(defun erc-can--write-hunk (data)
  "Write DATA to current buffer."
  (when-let ((hunk (plist-get data :hunk)))
    (with-current-buffer (plist-get data :buf)
      (goto-char (point-max))
      (let ((print-escape-control-characters t))
        (erc-can--ugly-print (nreverse hunk))))))

(defvar erc-can--buffers nil)
(defvar erc-can--fixed-timeout-in nil)

(defun erc-can--convert-in ()
  "Handle incoming line."
  (let* ((date (match-string 1))
         (name (match-string 2))
         (msg (match-string 3))
         (command (match-string 4))
         (ts (encode-time (iso8601-parse date t)))
         data)
    ;; Initialize new incoming
    (unless (setq data (assoc-default name erc-can--buffers))
      (setq data (list :ts ts
                       :hunk nil
                       :buf (erc-can--convert-setup-output-buffer name)))
      (push (cons name data) erc-can--buffers))
    (unless (member command erc-can--ignored-requests)
      ;; Write out last one
      (erc-can--write-hunk data)
      (setq data (plist-put data :hunk nil))
      (push (list (intern (downcase command))
                  (or erc-can--fixed-timeout-in
                      (max 1 (erc-can--diff-time (plist-get data :ts) ts)))
                  msg)
            (plist-get data :hunk))
      (setq data (plist-put data :ts ts)))
    (setf (alist-get name erc-can--buffers nil nil #'equal) data)))

(defun erc-can--convert-out ()
  "Prepare outgoing line."
  (let* ((date (match-string 1))
         (name (match-string 2))
         (msg (match-string 3))
         (ts (encode-time (iso8601-parse date t)))
         data)
    (unless (setq data (assoc-default name erc-can--buffers))
      (setq data (list :ts ts
                       :hunk nil
                       :buf (erc-can--convert-setup-output-buffer name)))
      (push (cons name data) erc-can--buffers)
      (push (list 'FIXME-orphan 1 "FIXME orphan")
            (plist-get data :hunk)))
    (let ((p (erc-d-i--parse-message msg t)))
      (unless (member (erc-d-i-message.command p) erc-can--ignored-replies)
        (push (list (let ((n (erc-can--diff-time (plist-get data :ts) ts)))
                      (when erc-can--float-max-out
                        (while (> n erc-can--float-max-out)
                          (setq n (/ n 10))))
                      n)
                    (erc-d-i-message.unparsed p))
              (plist-get (alist-get name erc-can--buffers nil nil #'equal)
                         :hunk))
        (setf (plist-get (alist-get name erc-can--buffers nil nil #'equal) :ts)
              ts)))))

(defun erc-can-convert-protocol-log (file)
  "Read erc-protocol-log FILE and save as formatted dialog."
  (interactive "ffile: ")
  (setq erc-can--buffers nil)
  (with-temp-buffer
    (set-buffer-multibyte nil)
    (insert-file-contents-literally file)
    (while (zerop (forward-line))
      (or (when (looking-at erc-can--protocol-regexp-in)
            (erc-can--convert-in))
          (when (looking-at erc-can--protocol-regexp-out)
            (erc-can--convert-out)))))
  (pcase-dolist (`(,_name . ,data) erc-can--buffers)
    (erc-can--write-hunk data)))

(defun erc-can-convert ()
  "Run `erc-can-convert-protocol-log' from the command line.

Usage: [--timeout IN-SECS] [--float-fmt FMT] [--max-out OUT-SECS]
       [--destdir DIR] LOG

  DIR       destination directory
  FMT       format of outgoing delay secs
  IN-SECS   fixed timeout for incoming pattern matches
  OUT-SECS  max outgoing delay secs
  LOG       name of existing file containing *erc-protocol* output

To retain passwords, set `erc--debug-irc-protocol-mask-secrets' to nil
before connecting."
  (let (source destdir timeout floatfmt maxout die erc-can--buffers)
    (while command-line-args-left
      (pcase (pop command-line-args-left)
        ("--destdir" (setq destdir (pop command-line-args-left)))
        ("--float-fmt" (setq floatfmt (pop command-line-args-left)))
        ("--timeout" (setq timeout
                           (string-to-number (pop command-line-args-left))))
        ("--max-out" (setq maxout
                           (string-to-number (pop command-line-args-left))))
        ((and (rx bot "--") v) (message "Unknown flag: %S" v) (setq die t))
        ((and (guard (null source)) v) (setq source v))))
    (unless (and source (file-readable-p source))
      (setq die t))
    (when die
      (princ (function-documentation (symbol-function 'erc-can-convert)))
      (kill-emacs 1))
    (unless destdir
      (setq destdir (file-name-directory source)))
    (let ((erc-can--fixed-timeout-in (or timeout erc-can--fixed-timeout-in))
          (erc-can--float-format-out (or floatfmt erc-can--float-format-out))
          (erc-can--float-max-out (or maxout erc-can--float-max-out)))
      (erc-can-convert-protocol-log source))
    (pcase-dolist (`(,name . ,data) erc-can--buffers)
      (setq name (format "%s.eld" (replace-regexp-in-string "/" "-" name)))
      (with-current-buffer (plist-get data :buf)
        (write-file (expand-file-name name destdir))))))

(provide 'erc-can)

;;; erc-can.el ends here
