
BUG ?= meta
SCENARIO ?= noop
TMPDIR ?= /tmp
OP ?= test

OPS = test itest live
OP ?= test

RIO_DIR = ../bugs/$(BUG)/scenarios/$(SCENARIO)

# File should contain "LOG_MANIFEST = ..." and ".PHONY: mybug"
include $(RIO_DIR)/inc.mk

LOG_DIR ?= $(TMPDIR)/erc-logs/$(BUG)/$(SCENARIO)
LOGS = $(addprefix $(LOG_DIR)/,$(LOG_MANIFEST))

PROG_test = -batch -l $(RIO_DIR)/drive.el -f repro-run-batch
PROG_itest = -l $(RIO_DIR)/drive.el -f repro-run-debug
PROG_live = -l repro/repro-i.el -f repro-run-inferior $(OPTIONS)
PROGRAM := $(PROG_$(OP))

.PHONY: clean

$(LOGS) &: $(addprefix $(RIO_DIR)/,steps.el drive.el) | $(LOG_DIR)
	$(EMACS) -Q -L repro $(PROGRAM) $(realpath $<) $(abspath $(LOG_DIR))

$(LOG_DIR):
	mkdir -p $@

# Must pass BUG=foo for this to work (might as well do manually)
clean:
	test -d $(LOG_DIR)
	rm -rf $(LOG_DIR)
