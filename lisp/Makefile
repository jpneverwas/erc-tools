
EMACS ?= emacs
SELECTOR ?= t
BUG_NUMBER ?= 49860

ifdef SCENARIO
include repro.mk
endif

TARBALL ?= ../archive/erc-$(BUG_NUMBER).tar

.PHONY: test-d test-any serve-d convert test-can test install fake test-i

fake:
	@echo nope
	false

# Installation expects a top-level directory named "archive" containing an
# ELPA package called erc-$BUG_NUMBER.tar
#
# XXX important: the erc-tests-compat library must NOT be loaded during
# installation because its definitions may end up compiled into ERC, thus
# leading to false negatives.
install:
	$(EMACS) -batch \
		-eval '(package-refresh-contents)' \
		-eval '(package-install-file "$(TARBALL)")'

define loadtests
(let* ((root (file-name-directory (locate-library "erc")))\
       (tests (expand-file-name "test" root)))\
  (dolist (f (directory-files tests t "erc-.*.el"))\
    (load f)))
endef

test: compat/erc-tests-compat.elc
	$(EMACS) -batch -Q -L compat \
		-eval '(require (quote erc-tests-compat))' \
		-eval '(package-initialize)' \
		-eval '$(loadtests)' \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

# Start interactive Emacs with all tests defined.
test-i: compat/erc-tests-compat.elc
	$(EMACS) -Q -L compat \
		-eval '(setq make-backup-files nil)' \
		-eval '(require (quote erc-tests-compat))' \
		-eval '(package-initialize)' \
		-eval '$(loadtests)'

compat/erc-tests-compat.elc :
	$(EMACS) -batch -Q -eval '(byte-recompile-directory "compat" 0 t)'

test-d:
	$(EMACS) -Q -batch -L erc-d \
		-l erc-d/erc-d-tests.el \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

test-any: LIB ?= erc-d/erc-d-tests.el
test-any:
	test -f $(LIB)
	$(EMACS) -Q -batch -L erc-d \
		-l $(LIB) \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

baked: NAME ?= meta/noop
baked: LIB ?= ../bugs/$(NAME)/scenarios/baked/$(PREFIX)test.el
baked: SELECTOR = (quote $(NAME))
baked:
	test -f $(LIB)
	$(EMACS) -Q -batch -L erc-d \
		-l $(LIB) \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

baked-broke: PREFIX = broke/
baked-broke: baked

baked-fixed: PREFIX = fixed/
baked-fixed: baked

serve-d: HOST ?= localhost
serve-d: PORT ?= 6667
serve-d: DIALOG ?= basic
serve-d:
	cd erc-d && \
	$(EMACS) -Q -batch -L . -l ./erc-d-tests.el \
		-f erc-d-serve \
		--host $(HOST) \
		--port $(PORT) \
		--add-time 10 \
		$(DIALOG)

convert: IN ?= /tmp/fake.log
convert: FLAGS ?=
convert: $(IN)
	$(EMACS) -Q -batch -L erc-can -L erc-d -l erc-can/erc-can.el \
		-f erc-can-convert --timeout 10 --float-fmt %.2f --max-out 0.1 \
		$(FLAGS) $(realpath $<)

test-can: SELECTOR = "^erc-can-.*"
test-can:
	$(EMACS) -Q -batch -L erc-can -L erc-d \
		-l erc-can/erc-can-test.el \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'
