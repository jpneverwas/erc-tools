;; This is the bug file
(pop-to-buffer (get-buffer-create "my-buffer"))
(emacs-lisp-mode)
(insert ";; This is the program running\n")
(dolist (c (append "3.14159265359" nil))
  (insert (string c))
  (sit-for 0.2))
(message "This is %s" (buffer-name))
(sleep-for 1)
