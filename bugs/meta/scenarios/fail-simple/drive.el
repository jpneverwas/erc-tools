
;; The purpose of this test case is to show that the script(1) files
;; are saved even when the test fails.
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior
   (repro-execute-bug-recipe)

   (ert-info ("Script buffer shows")
     (repro-term-await-buffer "steps.el" 2))

   (ert-info ("Numbers printed")
     (repro-term-await-content "3\n4\n5\n" 5))

   (ert-info ("Message present")
     (repro-term-await-message "Done counting" 5))

   (sleep-for 2)))
