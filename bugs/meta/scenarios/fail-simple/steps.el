;; This is should fail
(require 'cl-lib)
(dolist (n (number-sequence 1 10))
  (insert (format "\n%d" n))
  ;; Raise arith error
  (when (= n 8)
    (message "%s" (/ n 0)))
  (sit-for 0.2))
(message "Done counting")

