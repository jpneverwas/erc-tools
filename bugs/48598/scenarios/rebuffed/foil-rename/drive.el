
(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "This should be steps.el")

   (repro-execute-bug-recipe)

   (sleep-for 1)

   (ert-info ("Server buffer barnet renamed")
     (repro-term-eval '(pop-to-buffer-same-window "barnet"))
     (repro-term-await-buffer "barnet" 10)
     (repro-term-speak 4 "Barnet's buffer is renamed: %S" '(buffer-name)))

   (ert-info ("Server buffer foonet renamed")
     (repro-term-eval '(pop-to-buffer-same-window "foonet"))
     (repro-term-await-buffer "foonet" 10)
     (repro-term-speak 4 "Foonet's buffer is renamed: %S" '(buffer-name)))

   (ert-info ("Successfully joined to #bar@barnet")
     (repro-term-eval '(pop-to-buffer-same-window "#bar"))
     (repro-term-await-buffer "#bar@barnet" 8)
     (repro-term-speak 5 "Channel #bar is healthy"))

   (ert-info ("Successfully joined to #foo@foonet")
     (repro-term-eval '(pop-to-buffer-same-window "#foo"))
     (repro-term-await-buffer "#foo@foonet" 8)
     (repro-term-speak 5 "Channel #foo is healthy"))

   (ert-info ("Reckon")
     (repro-term-ibuffer)
     (repro-term-speak 5 "There are two servers and two channels"))))
