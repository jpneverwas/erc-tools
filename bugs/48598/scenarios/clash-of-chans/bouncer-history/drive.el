;; -*- lexical-binding: t; -*-

(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Watch #chan for a bit")
     (repro-term-await-buffer "#chan@foonet" 20)
     (repro-term-speak 4 "This should be #chan@foonet %S" 'erc-server-process)
     (repro-term-await-buffer "#chan@barnet" 8)
     (repro-term-speak 8 "As well as #chan@barnet %S" 'erc-server-process)
     (repro-term-send "\e<")
     (repro-term-speak 4 "There should be some history playback from both")
     (while (not (repro-term-content-p "<\\(joe\\|mike\\)>"))
       (repro-term-send "\C-v")
       (sleep-for 0.2))
     (repro-term-send "\C-v")
     (sleep-for 3)
     (repro-term-send "\e>"))

   (ert-info ("Quit barnet")
     (repro-term-eval '(progn (switch-to-buffer "127.0.0.1:6670/127.0.0.1<2>")
                              (beginning-of-buffer)))
     (repro-term-await-buffer "barnet" 8)
     (repro-term-speak 3 "Let's quit %S ..." 'erc-network)
     (repro-term-send "\e>/quit")
     (sleep-for 1)
     (repro-term-send "\r")
     (repro-term-speak 6 "Intermingling ceases, only foonet folk remain"))

   (ert-info ("Reckon")
     (sleep-for 5)
     (repro-term-eval '(ibuffer))
     (repro-term-speak 5 "There should be two servers and one #chan"))

   (sleep-for 2)))

;; TODO:
;; - show that ERC treats each back/forth as joins
;; - show that an empty buffer name 127.0.0.1:6670 existed at some point but
;; - show that process-name numeric suffixes like <1> don't match those of
;;   buffer names
