(require 'erc)
(setq erc-rename-buffers t)

(cl-macrolet ((at (d &rest b) `(run-at-time ,d nil (lambda () (progn ,@b)))))

  (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                  :port 6670
                                  :nick "tester"
                                  :password "tester@vanilla/foonet:changeme"
                                  :full-name "tester"))

  (at 3 (execute-kbd-macro "/join #chan"))
  (at 5 (execute-kbd-macro "\r"))

  (at 12 (pop-to-buffer-same-window
          (erc :server "127.0.0.1"
               :port 6670
               :nick "tester"
               :password "tester@vanilla/barnet:changeme"
               :full-name "tester")))

  (at 16 (execute-kbd-macro "/join #chan"))
  (at 18 (execute-kbd-macro "\r")))
