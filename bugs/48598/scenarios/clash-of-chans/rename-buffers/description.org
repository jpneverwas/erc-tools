
:PROPERTIES:
:Source: #erc
:Originator: N/A
:Subject: Channel buffers still collide despite erc-rename-buffers
:Date: N/A
:Proxy: true
:Playback: false
:END:

* Description
Ensure the option `erc-rename-buffers' is non-nil. Connect to an IRC
network named foonet via a bouncer. The server buffer is correctly
renamed to match the network "foonet." Join a channel named #chan.
Connect to a different network, barnet, via the same bouncer. This
server buffer is also renamed correctly. Join a channel of the same
name, #chan, on this network. Another buffer is not created. The
existing buffer is not renamed. Output in #chan is intermingled,
displaying content from both networks.

This scenario is identical to "clash-of-chans/bouncer-history," except
for the lack of playback and the `erc-rename-buffers' option.
