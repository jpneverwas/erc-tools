;; -*- lexical-binding: t; -*-

(require 'repro)

(ert-deftest repro-test ()
  (repro-with-inferior

   (repro-term-speak 3 "Going in 3 ...")

   (repro-execute-bug-recipe)

   (ert-info ("Wait for foo")
     (repro-term-await-buffer "foonet" 10)
     (repro-term-speak 3 "Foonet's buffer is renamed: %S" '(buffer-name)))

   (ert-info ("Join #chan@foonet")
     (repro-term-await-buffer "#chan@foonet" 8)
     (repro-term-speak 5 "Output is exclusive to #chan@foonet"))

   (ert-info ("Wait for bar")
     (repro-term-await-buffer "barnet" 10)
     (repro-term-speak 3 "Barnet's buffer is renamed: %S" '(buffer-name)))

   (ert-info ("Join #chan@barnet")
     (repro-term-await-buffer "#chan@barnet" 8)
     (repro-term-speak 9 "The conversation is intermingled"))

   (ert-info ("Reckon")
     (repro-term-ibuffer)
     (repro-term-speak 5 "There are two servers and one #chan"))

   (sleep-for 2)))
