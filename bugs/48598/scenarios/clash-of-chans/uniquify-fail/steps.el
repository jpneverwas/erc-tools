(require 'erc)
(setq erc-reuse-buffers nil
      erc-modules (cons 'scrolltobottom (delq 'autojoin erc-modules)))

(erc :server "127.0.0.1"
     :port 6670
     :nick "tester"
     :password "tester@vanilla/foonet:changeme"
     :full-name "tester")

(sleep-for 2)

(erc :server "127.0.0.1"
     :port 6670
     :nick "tester"
     :password "tester@vanilla/barnet:changeme"
     :full-name "tester")
