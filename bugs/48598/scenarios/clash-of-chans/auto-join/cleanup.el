(require 'erc)

(defun /my/cleanup (name)
  (erc :server "127.0.0.1"
       :port 6670
       :nick "tester"
       :password (concat "tester@vanilla/" name ":changeme")
       :full-name "tester")
  (sleep-for 2)
  (erc-with-server-buffer
   (when (get-buffer "#chan")
     (erc-cmd-PART "#chan"))
   (sleep-for 2)
   (erc-cmd-QUIT "")
   (sleep-for 1)))

(/my/cleanup "foonet")
(/my/cleanup "barnet")
(if noninteractive
    (kill-emacs 0)
  (ibuffer))
