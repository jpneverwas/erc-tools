From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Tue, 30 Jan 2024 18:14:50 -0800
Subject: [PATCH 10/37] [5.7] Persist erc-ring-input across sessions

* lisp/erc/erc-ring.el (erc-ring-ignoredups): New option.
(erc-ring-mode, erc-ring-enable, erc-ring-disable): Run buffer-local
setup and teardown immediately or on major-mode hook if inside
`erc-open'.
(erc-ring--setup): New function, based on old body of
`erc-input-ring-setup'.
(erc-input-ring-setup): Defer to `erc-ring--setup', but only perform
setup.  Explain nonstandard buffer-local behavior.
(erc-add-to-input-ring): Honor `erc-ring-ignoredups'.
* test/lisp/erc/erc-tests.el (erc-ring-previous-command-base-case):
Assert nonstandard buffer-local behavior instead of regurgitating the
implementation, which is an exercise in abstraction leakage.
(erc-ring-previous-command): Use helpers from common test library.
(erc-ring-ignoredups): New test.
---
 etc/ERC-NEWS               |   4 ++
 lisp/erc/erc-ring.el       |  49 ++++++++++++++---
 test/lisp/erc/erc-tests.el | 104 ++++++++++++++++++++++++++++++-------
 3 files changed, 129 insertions(+), 28 deletions(-)

diff --git a/etc/ERC-NEWS b/etc/ERC-NEWS
index bef66381ae0..23532757a1b 100644
--- a/etc/ERC-NEWS
+++ b/etc/ERC-NEWS
@@ -44,6 +44,10 @@ Born from the ashes of a PoC drafted nearly a quarter century ago, the
 options to various "scopes" of interest based on match conditions
 reminiscent of 'buffer-match-p'.
 
+** Input ring history survives reconnects.
+In related news, new option 'erc-ring-ignoredups' tells 'erc-ring' to
+deduplicate adjacent history entries.
+
 ** Changes in the library API.
 
 *** Major-mode setup runs before global-module setup in 'erc-open'.
diff --git a/lisp/erc/erc-ring.el b/lisp/erc/erc-ring.el
index 2a914b7657b..ae8acf78f34 100644
--- a/lisp/erc/erc-ring.el
+++ b/lisp/erc/erc-ring.el
@@ -47,12 +47,20 @@ ring
   "Stores input in a ring so that previous commands and messages can
 be recalled using M-p and M-n."
   ((add-hook 'erc--input-review-functions #'erc-add-to-input-ring 90)
+   (erc-with-initialized-session (erc-ring--setup))
    (define-key erc-mode-map "\M-p" #'erc-previous-command)
    (define-key erc-mode-map "\M-n" #'erc-next-command))
   ((remove-hook 'erc--input-review-functions #'erc-add-to-input-ring)
+   (erc-buffer-do #'erc-ring--setup)
    (define-key erc-mode-map "\M-p" #'undefined)
    (define-key erc-mode-map "\M-n" #'undefined)))
 
+(defcustom erc-ring-ignoredups nil
+  "Merge adjacent repeated entries in `erc-input-ring'.
+Analogous to `comint-input-ignoredups'."
+  :package-version '(ERC . "5.7") ; FIXME sync on release
+  :type 'boolean)
+
 (defvar-local erc-input-ring nil "Input ring for erc.")
 
 (defvar-local erc-input-ring-index nil
@@ -62,20 +70,45 @@ erc-input-ring-index
 line is non-blank and displays the item from the ring indexed by this
 variable.")
 
+(defun erc-ring--setup ()
+  "Initialize or destroy local comint-style input ring.
+When `erc-ring-mode' is disabled, kill local vars.  Otherwise,
+initialize them unless an `erc-input-ring' already exists, in which case
+set `erc-input-ring-index' to nil."
+  (cond ((not erc-ring-mode)
+         (kill-local-variable 'erc-input-ring)
+         (kill-local-variable 'erc-input-ring-index))
+        ((ring-p erc-input-ring)
+         (setq erc-input-ring-index nil))
+        (t ; restore from previous local vars during `erc-open'
+         (let* ((priors (or erc--server-reconnecting erc--target-priors))
+                (existing (and priors (alist-get 'erc-input-ring priors))))
+           (setq erc-input-ring
+                 (or existing (make-ring comint-input-ring-size))
+                 erc-input-ring-index
+                 (and existing (alist-get 'erc-input-ring-index priors)))))))
+
 (defun erc-input-ring-setup ()
-  "Do the setup required so that we can use comint style input rings.
-Call this function when setting up the mode."
-  (unless (ring-p erc-input-ring)
-    (setq erc-input-ring (make-ring comint-input-ring-size)))
-  (setq erc-input-ring-index nil))
+  "Ensure `erc-input-ring' has been initialized in the current buffer.
+Also reset `erc-input-ring-index' to nil in the current buffer.
+Do this even if `erc-stamp-mode' is disabled, and don't enable it
+either.  That is, ensure compatibility with pre-5.7 code that expects
+this and other `erc-ring' library functions to manage an input ring in
+ERC buffers without global module activation."
+  (let ((erc-ring-mode t))
+    (erc-ring--setup)))
 
 (defun erc-add-to-input-ring (state-or-string)
   "Add STATE-OR-STRING to input ring and reset history position.
 STATE-OR-STRING should be a string or an erc-input object."
   (unless erc-input-ring (erc-input-ring-setup))
-  (ring-insert erc-input-ring (if (erc-input-p state-or-string)
-                                  (erc-input-string state-or-string)
-                                state-or-string)) ; string
+  (when-let* ((input (if (erc-input-p state-or-string)
+                         (erc-input-string state-or-string)
+                       state-or-string))
+              ((or (not erc-ring-ignoredups)
+                   (ring-empty-p erc-input-ring)
+                   (not (equal input (ring-ref erc-input-ring 0))))))
+    (ring-insert erc-input-ring input))
   (setq erc-input-ring-index nil))
 
 (defun erc-clear-input-ring ()
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index 3d9e2158166..5ff2b5d8344 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -1329,33 +1329,33 @@ erc--modify-local-map
         (should-not calls)))))
 
 (ert-deftest erc-ring-previous-command-base-case ()
-  (ert-info ("Create ring when nonexistent and do nothing")
-    (let (erc-input-ring
-          erc-input-ring-index)
-      (erc-previous-command)
-      (should (ring-p erc-input-ring))
-      (should (zerop (ring-length erc-input-ring)))
-      (should-not erc-input-ring-index)))
+  (erc-mode)
+  (ert-info ("Create ring and enable mode indirectly, via command")
+    (erc-ring-mode -1)
+    ;; When a user issues an `erc-{previous,next}-command' and
+    ;; `erc-ring-mode' isn't enabled, still arrange to maintain a ring
+    ;; in the current buffer even without global module activation.
+    (erc-previous-command)
+    (should-not erc-ring-mode)
+    (should-not erc-input-ring-index)
+    (should (ring-p erc-input-ring))
+    (should (zerop (ring-length erc-input-ring))))
+  (erc-ring-mode -1)
   (should-not erc-input-ring))
 
 (ert-deftest erc-ring-previous-command ()
-  (with-current-buffer (get-buffer-create "*#fake*")
-    (erc-mode)
-    (erc-tests-common-prep-for-insertion)
-    (setq erc-server-current-nick "tester")
+  (erc-tests-common-make-server-buf)
+  (erc-ring-mode +1)
+  (setq erc-server-current-nick "tester")
+  (with-current-buffer (erc--open-target "#fake")
     (setq-local erc-last-input-time 0)
-    (should-not (local-variable-if-set-p 'erc-send-completed-hook))
-    (setq-local erc-send-completed-hook nil) ; skip t (globals)
-    ;; Just in case erc-ring-mode is already on
-    (setq-local erc--input-review-functions erc--input-review-functions)
-    (add-hook 'erc--input-review-functions #'erc-add-to-input-ring)
-    ;;
+
     (cl-letf (((symbol-function 'erc-process-input-line)
                (lambda (&rest _)
                  (erc-display-message
                   nil 'notice (current-buffer) "echo: one\n")))
-              ((symbol-function 'erc-command-no-process-p)
-               (lambda (&rest _) t)))
+              ((symbol-function 'erc-command-no-process-p) #'always))
+
       (ert-info ("Create ring, populate, recall")
         (insert "/one")
         (erc-send-current-line)
@@ -1370,6 +1370,7 @@ erc-ring-previous-command
         (erc-bol)
         (should (looking-at "/one"))
         (should (zerop erc-input-ring-index)))
+
       (ert-info ("Back to one")
         (should (= (ring-length erc-input-ring) (1+ erc-input-ring-index)))
         (erc-previous-command)
@@ -1377,6 +1378,7 @@ erc-ring-previous-command
         (erc-bol)
         (should (looking-at "$"))
         (should (equal (ring-ref erc-input-ring 0) "/one")))
+
       (ert-info ("Swap input after prompt with previous (#bug46339)")
         (insert "abc")
         (erc-previous-command)
@@ -1388,8 +1390,70 @@ erc-ring-previous-command
         (erc-next-command)
         (erc-bol)
         (should (looking-at "abc")))))
+
+  (erc-ring-mode -1)
+  (when noninteractive
+    (erc-tests-common-kill-buffers)))
+
+(ert-deftest erc-ring-ignoredups ()
+  (erc-tests-common-make-server-buf)
+  (erc-ring-mode +1)
+  (setq erc-server-current-nick "tester")
+  (with-current-buffer (erc--open-target "#fake")
+    (erc-tests-common-add-cmem "tester")
+    (setq-local erc-last-input-time 0)
+
+    (cl-letf* ((counter 0)
+               (erc-accidental-paste-threshold-seconds nil)
+               ((symbol-function 'erc-process-input-line)
+                (lambda (&rest _)
+                  (erc-display-msg
+                   (format "count: %d\n" (cl-incf counter)))))
+               ((symbol-function 'erc-command-no-process-p) #'always))
+
+      (ert-info ("Baseline")
+        (should-not erc-ring-ignoredups)
+        (ert-info ("Insert /one") (insert "/one") (erc-send-current-line))
+        (ert-info ("Insert /two") (insert "/two") (erc-send-current-line))
+        (ert-info ("Repeat /two") (insert "/two") (erc-send-current-line))
+        (save-excursion
+          (goto-char (point-min))
+          (should (search-forward "<tester> count: 1" nil t))
+          (should (search-forward "<tester> count: 2" nil t))
+          (should (search-forward "<tester> count: 3" nil t)))
+        (ert-info ("C-p -1") (erc-previous-command) (erc-bol))
+        (should (looking-at "/two"))
+        (ert-info ("C-p -2") (erc-previous-command) (erc-bol))
+        (should (looking-at "/two"))
+        (ert-info ("C-p -3") (erc-previous-command) (erc-bol))
+        (should (looking-at "/one"))
+        (ert-info ("C-p -4") (erc-previous-command) (erc-bol))
+        (should (looking-at "$")))
+
+      ;; Cycle mode.
+      (erc-ring-mode -1)
+      (erc-ring-mode +1)
+
+      (ert-info ("With option")
+        (let ((erc-ring-ignoredups t))
+          (ert-info ("Insert /one") (insert "/one") (erc-send-current-line))
+          (ert-info ("Insert /two") (insert "/two") (erc-send-current-line))
+          (ert-info ("Repeat /two") (insert "/two") (erc-send-current-line))
+          (save-excursion
+            (goto-char (point-min))
+            (should (search-forward "<tester> count: 4" nil t))
+            (should (search-forward "<tester> count: 5" nil t))
+            (should (search-forward "<tester> count: 6" nil t)))
+          (ert-info ("C-p -1") (erc-previous-command) (erc-bol))
+          (should (looking-at "/two"))
+          (ert-info ("C-p -2") (erc-previous-command) (erc-bol))
+          (should (looking-at "/one"))
+          (ert-info ("C-p -3") (erc-previous-command) (erc-bol))
+          (should (looking-at "$"))))))
+
+  (erc-ring-mode -1)
   (when noninteractive
-    (kill-buffer "*#fake*")))
+    (erc-tests-common-kill-buffers)))
 
 (ert-deftest erc--debug-irc-protocol-mask-secrets ()
   (should-not erc-debug-irc-protocol)
-- 
2.48.1

