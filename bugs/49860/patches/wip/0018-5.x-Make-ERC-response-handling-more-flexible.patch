From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Fri, 17 Jun 2022 01:43:03 -0700
Subject: [PATCH 18/37] [5.x] Make ERC response handling more flexible

* lisp/erc/erc-backend.el (erc--parsed-response): New variable to be
the official internal version of the ancient `erc-message-parsed',
which is only available during `erc-display-message', and
quasi-deprecated.
(erc-call-hooks): Bind `erc--parsed-response' to the parsed
`erc-response' object for the duration of its handling.  Bind
`erc--msg-prop-overrides' around all hooks.
(erc--generic-response-handlers): Add new constant listing IRC
commands and "numerics" whose handler logic should live in an internal
generic method to be called by a traditional erc-server-FOO handler,
now just a thin public wrapper.  ERC matches these against the NAME
param (but not ALIASES) of `define-erc-response-handler'.
(define-erc-response-handler): Allow defining a handler as an internal
defgeneric to be called by a public handler.  The rationale is that
the existing public hook interface cannot adequately support a
context-driven feature set whose behavior hinges on negotiated
extensions.  For example, registering a local
`erc-server-FOO-functions' member at a negative hook depth and
returning non-nil based on which extensions are active would still
preempt existing user functions registered to run after a primary
`erc-server-FOO' handler.  Also tweak edebug spec in declare form so
it doesn't error on encountering an IRC numeric.  (Bug#49860)
---
 lisp/erc/erc-backend.el | 15 ++++++++++++++-
 1 file changed, 14 insertions(+), 1 deletion(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index 53d73be525a..5806da3492e 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -1642,6 +1642,9 @@ erc-handle-unknown-server-response
                                  " "))))
     (erc-display-message parsed 'notice proc line)))
 
+(eval-and-compile
+  (defconst erc--generic-response-handlers
+    '(JOIN 005 263 352 353 354 366 900 903 904 CAP)))
 
 (cl-defmacro define-erc-response-handler ((name &rest aliases)
                                           &optional extra-fn-doc extra-var-doc
@@ -1775,6 +1778,9 @@ define-erc-response-handler
                              (concat extra-fn-doc "\n\n")
                            "")
                          name hook-name))
+         (fn-internal (when (memq (read (symbol-name name))
+                                  erc--generic-response-handlers)
+                        (intern (format "erc--server-%s" name))))
          (fn-alternates
           (cl-loop for alias in aliases
                    collect (intern (format "erc-server-%s" alias))))
@@ -1787,11 +1793,18 @@ define-erc-response-handler
          ;; unconditionally
          (defvar ,hook-name nil ,(format hook-doc name))
          (add-hook ',hook-name #',fn-name)
+
+       ,@(when fn-internal
+           `((cl-defgeneric ,fn-internal (proc parsed)
+               ,(or extra-fn-doc (format "Handle incoming %s response." name))
+               (ignore proc parsed)
+               ,@fn-body))) ; FIXME indentation in form just below
+
          ;; Handler function
          (defun ,fn-name (proc parsed)
            ,fn-doc
            (ignore proc parsed)
-           ,@fn-body)
+           ,@(if fn-internal `((,fn-internal proc parsed)) fn-body))
 
        ;; Make find-function and find-variable find them
        (put ',fn-name 'definition-name ',name)
-- 
2.48.1

