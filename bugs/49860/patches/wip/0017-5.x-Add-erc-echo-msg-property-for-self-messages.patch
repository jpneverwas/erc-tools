From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sun, 20 Oct 2024 18:05:14 -0700
Subject: [PATCH 17/37] [5.x] Add erc--echo msg property for self messages

* lisp/erc/erc-backend.el (erc-server-PRIVMSG): Add `erc--echo' prop
when sender has client's current nick.  Pass inputp to alternate
`erc--ctcp-response' constructor.
* lisp/erc/erc-common.el (erc--ctcp-response): Add `inputp' slot.
* lisp/erc/erc.el (erc--msg-props): Mention `erc--echo'.
(erc--send-message-external): Convert to generic.
(erc-process-ctcp-query): Skip all response handling for echoed
non-ACTION queries, like VERSION, because no spoofed prompt input needs
inserting, and no replies need emitting.
---
 lisp/erc/erc-backend.el |  5 ++++-
 lisp/erc/erc-common.el  |  5 +++--
 lisp/erc/erc.el         | 36 +++++++++++++++++++++---------------
 3 files changed, 28 insertions(+), 18 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index 52d8b4c2568..53d73be525a 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -2203,13 +2203,16 @@ erc--speaker-status-prefix-wanted-p
                                          erc-show-speaker-membership-status
                                          inputp)
                                      (cdr cmem))))))
+        (when inputp
+          (push (cons 'erc--echo t) erc--msg-prop-overrides))
         (if (erc-is-message-ctcp-p msg)
             (if noticep
                 (erc-process-ctcp-reply proc parsed nick login host
                                         (match-string 1 msg))
               (setq parsed (erc--ctcp-response-from-parsed
                             :parsed parsed :buffer buffer :statusmsg statusmsg
-                            :prefix cmem-prefix :dispname fnick))
+                            :prefix cmem-prefix :dispname fnick
+                            :inputp inputp))
               (erc-process-ctcp-query proc parsed nick login host))
           (setq erc-server-last-peers (cons nick (cdr erc-server-last-peers)))
           (with-current-buffer (or buffer (current-buffer))
diff --git a/lisp/erc/erc-common.el b/lisp/erc/erc-common.el
index a973750efef..26c656d8c50 100644
--- a/lisp/erc/erc-common.el
+++ b/lisp/erc/erc-common.el
@@ -135,7 +135,7 @@ erc--target
                (:include erc-response)
                (:constructor
                 erc--ctcp-response-from-parsed
-                (&key parsed buffer statusmsg prefix dispname
+                (&key parsed buffer statusmsg prefix dispname inputp
                       &aux (unparsed (erc-response.unparsed parsed))
                       (sender (erc-response.sender parsed))
                       (command (erc-response.command parsed))
@@ -146,7 +146,8 @@ erc--target
   (buffer nil :type (or buffer null))
   (statusmsg nil :type (or null string))
   (prefix nil :type (or erc-channel-user null))
-  (dispname nil :type (or string null)))
+  (dispname nil :type (or string null))
+  (inputp nil :type boolean))
 
 (cl-defstruct erc--isupport-data
   "Abstract \"class\" for parsed ISUPPORT data.
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index eebae15a681..b6382448e61 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -191,6 +191,9 @@ erc--msg-props
     hooks that the current message should not affect stateful
     operations, such as recording a channel's most recent speaker
 
+ - `erc--echo': t when present; indicates message is an echoed copy
+    of a something originally spoken by the client.
+
  - `erc--hide': a symbol or list of symbols added as an `invisible'
     prop value to the entire message, starting *before* the preceding
     newline and ending before the trailing newline
@@ -5186,7 +5189,7 @@ erc-send-message
       (funcall erc--send-message-nested-function line force)
     (erc--send-message-external line force)))
 
-(defun erc--send-message-external (line force)
+(cl-defmethod erc--send-message-external (line force)
   "Send a \"PRIVMSG\" to the default target with optional FORCE.
 Expect caller to bind `erc-default-recipients' if needing to
 specify a status-prefixed target."
@@ -6925,7 +6928,9 @@ erc-process-ctcp-query
   ;; FIXME: This needs a proper docstring -- Lawrence 2004-01-08
   "Process a CTCP query."
   (let ((queries (delete "" (split-string (erc-response.contents parsed)
-                                          "\C-a"))))
+                                          "\C-a")))
+        (echoedp (and (erc--ctcp-response-p parsed)
+                      (erc--ctcp-response-inputp parsed))))
     (if (> (length queries) 4)
         (erc-display-message
          parsed (list 'notice 'error) proc 'ctcp-too-many)
@@ -6944,20 +6949,21 @@ erc-process-ctcp-query
                      hook proc parsed nick login host
                      (car (erc-response.command-args parsed))
                      (car queries))
-                  (when erc-paranoid
-                    (if (erc-current-nick-p
-                         (car (erc-response.command-args parsed)))
+                  (unless echoedp
+                    (when erc-paranoid
+                      (if (erc-current-nick-p
+                           (car (erc-response.command-args parsed)))
+                          (erc-display-message
+                           parsed 'error 'active 'ctcp-request
+                           ?n nick ?u login ?h host ?r (car queries))
                         (erc-display-message
-                         parsed 'error 'active 'ctcp-request
-                         ?n nick ?u login ?h host ?r (car queries))
-                      (erc-display-message
-                       parsed 'error 'active 'ctcp-request-to
-                       ?n nick ?u login ?h host ?r (car queries)
-                       ?t (car (erc-response.command-args parsed)))))
-                  (run-hook-with-args-until-success
-                   hook proc nick login host
-                   (car (erc-response.command-args parsed))
-                   (car queries)))
+                         parsed 'error 'active 'ctcp-request-to
+                         ?n nick ?u login ?h host ?r (car queries)
+                         ?t (car (erc-response.command-args parsed)))))
+                    (run-hook-with-args-until-success
+                     hook proc nick login host
+                     (car (erc-response.command-args parsed))
+                     (car queries))))
               (erc-display-message
                parsed (list 'notice 'error) proc
                'undefined-ctcp)))
-- 
2.48.1

