From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Fri, 15 Jul 2022 05:02:35 -0700
Subject: [PATCH 26/37] [WIP] Retain references to all erc-data prop values

FIXME mention removal of channel user objects after display in
ERC-NEWS.
---
 lisp/erc/erc-backend.el           |  6 +-
 lisp/erc/erc-button.el            | 97 ++++++++++++++++++++++++++++---
 test/lisp/erc/erc-button-tests.el | 18 ++++++
 3 files changed, 110 insertions(+), 11 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index fd377df16f9..bbd112fc2f0 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -2895,11 +2895,11 @@ erc--extract-352-full-name
   "NAMES notice." nil
   (let ((channel (nth 2 (erc-response.command-args parsed)))
         (users (erc-response.contents parsed)))
+    (erc-with-buffer (channel proc)
+      (erc-channel-receive-names users))
     (erc-display-message parsed 'notice (or (erc-get-buffer channel proc)
                                             'active)
-                         's353 ?c channel ?u users)
-    (erc-with-buffer (channel proc)
-      (erc-channel-receive-names users))))
+                         's353 ?c channel ?u users)))
 
 (define-erc-response-handler (366)
   "End of NAMES." nil
diff --git a/lisp/erc/erc-button.el b/lisp/erc/erc-button.el
index 306ffd59e1e..ae779cd1eea 100644
--- a/lisp/erc/erc-button.el
+++ b/lisp/erc/erc-button.el
@@ -56,10 +56,12 @@ button
    (add-hook 'erc-send-modify-hook #'erc-button-add-buttons 30)
    (erc-with-initialized-session (erc-button-setup))
    (add-hook 'erc--tab-functions #'erc-button-next)
+   (erc-button--user-setup t)
    (erc--modify-local-map t "<backtab>" #'erc-button-previous))
   ((remove-hook 'erc-insert-modify-hook #'erc-button-add-buttons)
    (remove-hook 'erc-send-modify-hook #'erc-button-add-buttons)
    (remove-hook 'erc--tab-functions #'erc-button-next)
+   (erc-button--user-setup nil)
    (erc--modify-local-map nil "<backtab>" #'erc-button-previous)))
 
 ;;; Variables
@@ -416,16 +418,22 @@ erc-button--get-user-from-spkr-prop
 (cl-defstruct (erc--phantom-server-user (:include erc-server-user)))
 
 (defun erc-button--add-phantom-speaker (parts)
-  (pcase-let* (((cl-struct erc--msg-parts nick login host key) parts)
-               (cmem (gethash key erc-button--phantom-cmems))
-               (user (or (car cmem)
+  (pcase-let* (((cl-struct erc--msg-parts nick login host key parsed) parts)
+               (`(,du . ,dc) (erc-button--find-data-channel-member key))
+               (account (and-let* ((tags (erc-response.tags parsed)))
+                          (alist-get 'account tags)))
+               (user (or (and (erc--phantom-server-user-p du) du)
                          (make-erc--phantom-server-user
                           :nickname nick
-                          :host (and (not (string-empty-p host)) host)
-                          :login (and (not (string-empty-p login)) login))))
-               (cuser (or (cdr cmem)
+                          :host host
+                          :login login
+                          :account account)))
+               (cuser (or (and (erc--phantom-channel-user-p dc) dc)
                           (make-erc--phantom-channel-user
-                           :last-message-time (current-time)))))
+                           :last-message-time (erc--current-time-pair)))))
+    ;; Can't create `erc-data' object yet without access to the
+    ;; inserted nick, which we'd need to preserve a weak reference
+    ;; against `erc-button--nicks-to-data'.
     (puthash key (cons user cuser) erc-button--phantom-cmems)
     (cons user cuser)))
 
@@ -458,6 +466,78 @@ erc-button--phantom-users-mode
                      #'erc-button--get-phantom-cmem)
     (kill-local-variable 'erc-button--phantom-cmems)))
 
+(defun erc-button--drain-phantom-cmems ()
+  "Forget channel members in `erc-button--phanotom-users-mode'.
+Expect integrating modules to call this when needing to clear the
+table without running minor-mode hooks.  Before clearing, iterate
+over phantom channel members and update speaker times for all
+`erc-channel-members' of the same nick and non-nil account."
+  (when (and erc-button--phantom-users-mode
+             erc-button--phantom-cmems)
+    (map-do (lambda (k v)
+              (when-let* ((phcusr (cdr v))
+                          (phtime (erc-channel-user-last-message-time phcusr))
+                          (phacct (erc-server-user-account (car v)))
+                          (cmem (gethash k erc-channel-members))
+                          (cusr (cdr cmem))
+                          ((equal phacct (erc-server-user-account (car cmem)))))
+                (erc--update-cusr-message-time cusr phtime)))
+            erc-button--phantom-cmems)
+    (clrhash erc-button--phantom-cmems)))
+
+(defun erc-button--deemphasize-plebs (nick-object)
+  "Don't embolden nicks lacking status prefixes."
+  (when (and nick-object
+             (erc-button--nick-cusr nick-object)
+             (let ((cusr (nth 2 (erc-button--nick-data nick-object))))
+               (or (not cusr) (zerop (erc-channel-user-status cusr)))))
+    (setf (erc-button--nick-nickname-face nick-object) nil))
+  nick-object)
+
+(defvar-local erc-button--nicks-to-data nil
+  "Hash table mapping downcased nicks to `erc-data' text-prop values.
+Each value is a list of (NON-DOWNCASED SERVER-USER CHANNEL-USER).")
+
+(defun erc-button--initialize-users ()
+  (setq erc-button--nicks-to-data
+        (or (and erc--server-reconnecting
+                 (alist-get 'erc-button--nicks-to-data
+                            (or erc--target-priors erc--server-reconnecting)))
+            (make-hash-table :test #'equal :weakness 'value))))
+
+(defun erc-button--user-setup (modep)
+  (if modep
+      (progn (add-hook 'erc-mode-hook #'erc-button--initialize-users)
+             ;; Some local module that depends on this one is trying
+             ;; to activate it explicitly.  Or (unlikely), someone
+             ;; without `button' in their `erc-modules' decides to
+             ;; try it out during a live session.
+             (unless erc--updating-modules-p
+               (erc-with-all-buffers-of-server nil
+                   (lambda () (null erc-button--nicks-to-data))
+                 (erc-button--initialize-users))))
+    (remove-hook 'erc-mode-hook #'erc-button--initialize-users)
+    (erc-with-all-buffers-of-server nil nil
+      (kill-local-variable 'erc-button--nicks-to-data))))
+
+(defun erc-button--find-data-channel-member (downcased-nick)
+  "Return an `erc-channel-members' item for DOWNCASED-NICK."
+  (when-let* ((erc-button--nicks-to-data)
+              (found (gethash downcased-nick erc-button--nicks-to-data)))
+    (cons (nth 1 found) (nth 2 found))))
+
+(defun erc-button--ensure-data-prop (downcased server-user cusr)
+  "Remember SERVER-USER and CUSR for DOWNCASED nick."
+  (cl-assert server-user)
+  (let ((data (gethash downcased erc-button--nicks-to-data)))
+    (unless (eq server-user (nth 1 data))
+      (when erc--target
+        (cl-assert cusr))
+      (setq data (list (erc-server-user-nickname server-user) ; compat
+                       server-user cusr)) ; cmem
+      (puthash downcased data erc-button--nicks-to-data))
+    data))
+
 (defun erc-button-add-nickname-buttons (entry)
   "Search through the buffer for nicknames, and add buttons."
   (when-let* ((form (nth 2 entry))
@@ -487,7 +567,8 @@ erc-button-add-nickname-buttons
                                      down word bounds seen))))
              (user (or (and cmem (car cmem))
                        (and erc-server-users (gethash down erc-server-users))))
-             (data (list word)))
+             (data (and user (erc-button--ensure-data-prop down user
+                                                           (cdr cmem)))))
         (when (or (not (functionp form))
                   (and user
                        (setq nick-obj (funcall form (make-erc-button--nick
diff --git a/test/lisp/erc/erc-button-tests.el b/test/lisp/erc/erc-button-tests.el
index 6c3431eeba2..5d5feefd377 100644
--- a/test/lisp/erc/erc-button-tests.el
+++ b/test/lisp/erc/erc-button-tests.el
@@ -306,4 +306,22 @@ erc-button--display-error-notice-with-keys
         (kill-buffer "*Help*")
         (kill-buffer)))))
 
+(ert-deftest erc-button--ensure-data-prop ()
+  (erc-tests-common-init-server-proc "sleep" "1")
+  (erc-button--initialize-users)
+
+  ;; No account, cuser status zero
+  (let* ((user [erc-server-user "Bob" "h" "~b" nil nil nil "Robert" 0 nil])
+         (cuser [erc-channel-user 0 nil])
+         (result (erc-button--ensure-data-prop "bob" user cuser)))
+    (should (equal result `("Bob", user ,cuser)))
+    (should (equal (gethash "bob" erc-button--nicks-to-data) result)))
+
+  ;; Both account and cuser
+  (let* ((user [erc-server-user "Alice" "h" "~a" nil nil "alyssa" "APH" 0 nil])
+         (cuser [erc-channel-user 1 nil])
+         (result (erc-button--ensure-data-prop "alice" user cuser)))
+    (should (equal result `("Alice" ,user ,cuser)))
+    (should (equal (gethash "alice" erc-button--nicks-to-data) result))))
+
 ;;; erc-button-tests.el ends here
-- 
2.48.1

