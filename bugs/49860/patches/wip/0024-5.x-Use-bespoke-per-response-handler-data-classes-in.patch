From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Thu, 29 Feb 2024 21:14:53 -0800
Subject: [PATCH 24/37] [5.x] Use bespoke per-response-handler data classes in
 ERC

* lisp/erc/erc-backend.el (erc-call-hooks): Defer to specialized
parsing function for special response object if available.
(erc--zresponse): New struct.
(erc--ztargeted): New struct.
(erc--zstatused): New struct for PRIVMSG, NOTICE, and TAGMSG.
(erc--define-zresponse): New macro.
(erc--zJOIN): Define struct for storing structured JOIN data to be
shared among JOIN handlers.
(erc-server-JOIN): Use `erc--zJOIN'.
(erc--zKICK): New struct.
(erc-server-KICK): Use `erc--zKICK'.
(erc--zMODE): New struct.
(erc-server-MODE): Use `erc--zMODE'.
(erc--zNICK): New struct.
(erc--rename-query-buffer): New function.
(erc-server-NICK): Use `erc--zNICK' and `erc--rename-query-buffer'.
(erc--zPART): New struct.
(erc-server-KICK, erc-server-PART, erc-server-QUIT): Rearrange
updating of channel member data to run before and after display
so that nicks are properly buttonized.
(erc-server-PART): Fix thinko involving positional arguments where a
channel would be displayed twice when a trailing reason was absent.
Also, only do singular removals when someone else has parted.
(erc--zPRIVMSG): Define structure to encapsulate parsed PRIVMSG data.
(erc-server-PRIVMSG): Use zPRIVMSG accessors.
* test/lisp/erc/erc-tests.el (erc-message): Adapt tests to use
`erc--zPRIVMSG-from-response' function for parsing PRIVMSG responses.
* test/lisp/erc/erc-tests.el (erc--zPRIVMSG): New test.
---
 lisp/erc/erc-backend.el    | 301 ++++++++++++++++++++++++++++---------
 test/lisp/erc/erc-tests.el |  31 +++-
 2 files changed, 256 insertions(+), 76 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index de3ff284c7d..fd377df16f9 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -195,6 +195,9 @@ erc-verbose-server-ping
                   (channel nick new-nick
                            &optional add voice halfop op admin owner host
                            login full-name info update-message-time))
+(declare-function erc-update-current-channel-member "erc"
+                  (nick new-nick &optional addp voice halfop op admin owner
+                        host login full-name info update-message-time account))
 (declare-function erc-update-user-nick "erc"
                   (nick &optional new-nick host login full-name info account))
 
@@ -1658,6 +1661,9 @@ erc-call-hooks
         (erc--msg-prop-overrides erc--msg-prop-overrides)
         (hook (or (erc-get-hook (erc-response.command message))
                   'erc-default-server-functions)))
+    (when-let* ((zresp-makr (get hook 'erc--zresponse-constructor)))
+      (setq message (funcall zresp-makr message)
+            erc--parsed-response message))
     (run-hook-with-args-until-success hook process message)
     ;; Some handlers, like `erc-cmd-JOIN', open new targets without
     ;; saving excursion, and `erc-open' sets the current buffer.
@@ -1857,6 +1863,95 @@ define-erc-response-handler
                               `(defvar ,var #',fn-name ,(format hook-doc a))
                               `(put ',var 'definition-name ',hook-name))))))
 
+;; Abstract base response.
+(erc--define-lazy-struct (erc--zresponse (:include erc-response))
+  ( nuh (erc-parse-user (erc-response.sender parsed))
+    :type cons :lazy parsed
+    :documentation "Triple of (NICK USER HOST) from `erc-parse-user'.")
+  ( nick-d (erc-downcase (car (erc--zresponse-nuh parsed)))
+    :type string :lazy parsed
+    :documentation "Downcased nickname parsed from sender (source).")
+  ( userhost (let ((nick (car (erc--zresponse-nuh parsed))))
+               (if (string= nick (erc-response.sender parsed))
+                   nick
+                 (substring (erc-response.sender parsed) (1+ (length nick)))))
+    :type string :lazy parsed
+    :documentation "Original userhost from sender (source).")
+  ( mynick-d (erc-downcase (erc-current-nick))
+    :type string :lazy t
+    :documentation "Current (possibly stale) downcased nickname.")
+  ( self-p (string= (erc--zresponse-nick-d parsed)
+                    (erc--zresponse-mynick-d parsed))
+    :type boolean :lazy parsed
+    :documentation "Non-nil if client's current nick is the sender.")
+  ( time (erc--time-pair-from-time-tag parsed)
+    :type list :lazy parsed
+    :documentation "A lisp-time ticks-hertz pair if non-nil.")
+  ( time-str (or (alist-get 'time (erc-response.tags parsed))
+                 (format-time-string "%FT%T.%3NZ"
+                                     (or (erc--zresponse-time parsed)
+                                         erc--current-time-pair
+                                         (erc--current-lisp-time))
+                                     t))
+    :type string :lazy parsed
+    :documentation "Time string from `time' tag or `erc--current-lisp-time'."))
+
+(erc--define-lazy-struct (erc--ztargeted (:include erc--zresponse))
+  ( target (car (erc-response.command-args parsed))
+    :type string :lazy parsed
+    :documentation "Target of the message: a nick or a channel."))
+
+(erc--define-lazy-struct (erc--zstatused (:include erc--zresponse))
+  ( target (if (erc--zstatused-statusmsg parsed)
+               (substring (car (erc-response.command-args parsed)) 1)
+             (car (erc-response.command-args parsed)))
+    :type string :lazy parsed
+    :documentation "Target of the message: a nick or a channel.")
+  ( statusmsg (and-let* ((target (car (erc-response.command-args parsed)))
+                         ((erc--statusmsg-target target)))
+                (substring target 0 1))
+    :type (or string null) :lazy parsed
+    :documentation "Target's \"STATUSMSG\" prefix, if any."))
+
+(defmacro erc--define-zresponse (command-or-numeric &rest slots)
+  "Define a \"lazy\" data struct inheriting from `erc-response'.
+Look for and honor a plist of zero or more zresponse-compatible
+cl-struct OPTIONS at the beginning of SLOTS, followed by zero or more
+LAZYSLOTS, which should contain non-nil default fields for deferred
+initialization, and append a (:lazy parsed) keyword pair to each slot
+definition."
+  (declare (indent 1))
+  (setq command-or-numeric
+        (mapcar (lambda (c) (if (numberp c) (intern (format "%03i" c)) c))
+                (ensure-list command-or-numeric)))
+  (let* ((name (intern (format "erc--z%s" (car command-or-numeric))))
+         (constructor (intern (format "%s-from-parsed" name)))
+         (puts (mapcar (lambda (c)
+                         `(put ',(intern (format "erc-server-%s-functions" c))
+                               'erc--zresponse-constructor #',constructor))
+                       command-or-numeric))
+         (options `(,@(let (options)
+                        (while (keywordp (car slots))
+                          (push (list (pop slots) (pop slots)) options))
+                        (unless (assq :include options)
+                          (push '(:include erc--zresponse) options))
+                        options)
+                    (:constructor
+                     ,constructor
+                     (p &aux
+                        (unparsed (erc-response.unparsed p))
+                        (sender (erc-response.sender p))
+                        (command (erc-response.command p))
+                        (command-args (erc-response.command-args p))
+                        (contents (erc-response.contents p))
+                        (tags (erc-response.tags p))))))
+         (form (macroexpand
+                `(erc--define-lazy-struct (,name ,@options)
+                   ,@(mapcar (lambda (s) `(,@s :lazy parsed)) slots)))))
+    `(progn
+       ,@puts
+       ,@(macroexp-unprogn form))))
+
 (define-erc-response-handler (ERROR)
   "Handle an ERROR command from the server." nil
   (setq erc-server-error-occurred t)
@@ -1884,55 +1979,70 @@ erc--server-determine-join-display-context
       alist
     `((erc-buffer-display . JOIN) ,@alist)))
 
+(erc--define-zresponse (JOIN)
+  :include erc--ztargeted
+  ( buffer (if-let* ((target (erc--ztargeted-target parsed))
+                     ((erc--zresponse-self-p parsed))
+                     (erc--display-context
+                      (erc--server-determine-join-display-context
+                       target erc--display-context)))
+               (save-excursion (erc--open-target target))
+             (erc-get-buffer target erc-server-process))
+    :type buffer
+    :documentation "Buffer of the joined channel."))
+
+;; FIXME it's possible a JOIN message's target buffer already exists
+;; and is thought to be joined, even though it isn't.  This can
+;; happen, for example, when a bouncer loses its upstream connection
+;; and resends JOINs upon reconnecting.  Measures should be taken to
+;; update the joined status and run related hooks so that modules can
+;; react to such changes.
 (define-erc-response-handler (JOIN)
-  "Handle join messages."
+  "Handle \"JOIN\" messages."
   nil
   (let ((chnl (erc-response.contents parsed))
         (buffer nil))
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
+                 (erc--zJOIN-nuh parsed)))
       ;; strip the stupid combined JOIN facility (IRC 2.9)
       (if (string-match "^\\(.*\\)\^g.*$" chnl)
-          (setq chnl (match-string 1 chnl)))
-      (save-excursion
+          (setf chnl (match-string 1 chnl)
+                (erc--zJOIN-target parsed) chnl))
+      (with-current-buffer (setq buffer (erc--zJOIN-buffer parsed))
         (let ((args (cond
                      ;; If I have joined a channel
-                     ((erc-current-nick-p nick)
-                      (let ((erc--display-context
-                             (erc--server-determine-join-display-context
-                              chnl erc--display-context)))
-                        (setq buffer (erc--open-target chnl)))
-                      (when buffer
-                        (set-buffer buffer)
-                        (with-suppressed-warnings
-                            ((obsolete erc-add-default-channel))
-                          (erc-add-default-channel chnl))
-                        (setf (erc--target-channel-joined-p erc--target) t)
-                        (erc-server-send (format "MODE %s" chnl)))
-                      (erc-with-buffer (chnl proc)
-                        (erc-channel-begin-receiving-names))
+                     ((erc--zJOIN-self-p parsed)
+                      (with-suppressed-warnings
+                          ((obsolete erc-add-default-channel))
+                        (erc-add-default-channel chnl))
+                      (setf (erc--target-channel-joined-p erc--target) t)
+                      (erc-server-send (format "MODE %s" chnl))
+                      (erc-channel-begin-receiving-names)
                       (erc-update-mode-line)
                       (run-hooks 'erc-join-hook)
                       (list 'JOIN-you ?c chnl))
                      (t
-                      (setq buffer (erc-get-buffer chnl proc))
                       (list 'JOIN ?n nick ?u login ?h host ?c chnl)))))
-          (when buffer (set-buffer buffer))
-          (erc-update-channel-member chnl nick nick t nil nil nil nil nil host login)
+          (erc-update-current-channel-member nick nick t nil nil nil nil nil
+                                             host login)
           (unless (erc-current-nick-p nick)
             (erc--ensure-query-member nick))
-          ;; on join, we want to stay in the new channel buffer
-          ;;(set-buffer ob)
           (apply #'erc-display-message parsed 'notice buffer args))))))
 
+(erc--define-zresponse (KICK)
+  :include erc--ztargeted
+  ( buffer (erc-get-buffer (erc--ztargeted-target parsed) erc-server-process)
+    :type buffer
+    :documentation "Buffer of the affected channel."))
+
 (define-erc-response-handler (KICK)
   "Handle kick messages received from the server." nil
-  (let* ((ch (nth 0 (erc-response.command-args parsed)))
+  (let* ((ch (erc--zKICK-target parsed))
          (tgt (nth 1 (erc-response.command-args parsed)))
          (reason (erc-trim-string (erc-response.contents parsed)))
-         (buffer (erc-get-buffer ch proc)))
+         (buffer (erc--zKICK-buffer parsed)))
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
+                 (erc--zKICK-nuh parsed)))
       (cond
        ((string= tgt (erc-current-nick))
         (erc-display-message
@@ -1945,7 +2055,7 @@ erc--server-determine-join-display-context
         (with-suppressed-warnings ((obsolete erc-delete-default-channel))
           (erc-delete-default-channel ch buffer))
         (erc-update-mode-line buffer))
-       ((string= nick (erc-current-nick))
+       ((erc--zKICK-self-p parsed)
         (erc-display-message
          parsed 'notice buffer
          'KICK-by-you ?k tgt ?c ch ?r reason)
@@ -1956,30 +2066,38 @@ erc--server-determine-join-display-context
           (erc-remove-channel-member buffer tgt)))))
   nil)
 
+(erc--define-zresponse (MODE)
+  :include erc--ztargeted
+  ( buffer (and-let* (((not (erc--zMODE-user-p parsed)))
+                      (target (erc--ztargeted-target parsed))
+                      (buffer (erc-get-buffer target erc-server-process)))
+             (cl-assert (erc-channel-p buffer))
+             buffer)
+    :type (or null buffer)
+    :documentation "Buffer of the affected channel, if any.")
+  ( user-p (erc-current-nick-p (erc--ztargeted-target parsed))
+    :type boolean))
+
 (define-erc-response-handler (MODE)
   "Handle server mode changes." nil
-  (let ((tgt (car (erc-response.command-args parsed)))
+  (let ((tgt (erc--zMODE-target parsed))
         (mode (mapconcat #'identity (cdr (erc-response.command-args parsed))
                          " ")))
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
+                 (erc--zMODE-nuh parsed)))
       (erc-log (format "MODE: %s -> %s: %s" nick tgt mode))
-      ;; dirty hack
-      (let ((buf (cond ((erc-channel-p tgt)
-                        (erc-get-buffer tgt proc))
-                       ((string= tgt (erc-current-nick)) nil)
-                       ((erc-active-buffer) (erc-active-buffer))
-                       (t (erc-get-buffer tgt)))))
-        (with-current-buffer (or buf
-                                 (current-buffer))
+      (let ((buf (or (erc--zMODE-buffer parsed)
+                     (and (erc--zMODE-user-p parsed) (current-buffer))
+                     (error "Could not find buffer for %S" tgt))))
+        (with-current-buffer buf
           (erc--update-modes (cdr (erc-response.command-args parsed))))
-          (if (or (string= login "") (string= host ""))
-              (erc-display-message parsed 'notice buf
-                                   'MODE-nick ?n nick
-                                   ?t tgt ?m mode)
+        (if (or (string= login "") (string= host ""))
             (erc-display-message parsed 'notice buf
-                                 'MODE ?n nick ?u login
-                                 ?h host ?t tgt ?m mode)))))
+                                 'MODE-nick ?n nick
+                                 ?t tgt ?m mode)
+          (erc-display-message parsed 'notice buf
+                               'MODE ?n nick ?u login
+                               ?h host ?t tgt ?m mode)))))
   nil)
 
 (defun erc--wrangle-query-buffers-on-nick-change (old new)
@@ -2013,12 +2131,14 @@ erc--wrangle-query-buffers-on-nick-change
         (erc-update-mode-line)))
     buffers))
 
+(erc--define-zresponse (NICK))
+
 (define-erc-response-handler (NICK)
   "Handle nick change messages." nil
   (let ((nn (erc-response.contents parsed))
         bufs)
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
+                 (erc--zNICK-nuh parsed)))
       (setq bufs (erc-buffer-list-with-nick nick proc))
       (erc-log (format "NICK: %s -> %s" nick nn))
       ;; if we had a query with this user, make sure future messages will be
@@ -2031,7 +2151,7 @@ erc--wrangle-query-buffers-on-nick-change
         (cl-pushnew buf bufs))
       (erc-update-user-nick nick nn host login)
       (cond
-       ((string= nick (erc-current-nick))
+       ((erc--zNICK-self-p parsed)
         (cl-pushnew (erc-server-buffer) bufs)
         ;; Show message in all query buffers.
         (setq bufs (append (erc--query-list) bufs))
@@ -2054,10 +2174,18 @@ erc--wrangle-query-buffers-on-nick-change
         (erc-display-message parsed 'notice bufs 'NICK ?n nick
                              ?u login ?h host ?N nn))))))
 
+(erc--define-zresponse (PART)
+  :include erc--ztargeted
+  ( buffer (let ((target (erc--ztargeted-target parsed)))
+             (erc-get-buffer target erc-server-process))
+    :type buffer
+    :documentation "Buffer of the channel being parted."))
+
 (define-erc-response-handler (PART)
   "Handle part messages." nil
   (let* ((chnl (car (erc-response.command-args parsed)))
-         (reason (erc-trim-string (erc-response.contents parsed)))
+         (reason (erc-trim-string (and (cdr (erc-response.command-args parsed))
+                                       (erc-response.contents parsed))))
          (buffer (erc-get-buffer chnl proc)))
     (pcase-let ((`(,nick ,login ,host)
                  (erc-parse-user (erc-response.sender parsed))))
@@ -2068,7 +2196,7 @@ erc--wrangle-query-buffers-on-nick-change
                            'PART ?n nick ?u login
                            ?h host ?c chnl ?r (or reason ""))
       (cond
-       ((string= nick (erc-current-nick))
+       ((erc--zPART-self-p parsed)
         (run-hook-with-args 'erc-part-hook buffer)
         (erc-with-buffer
             (buffer)
@@ -2115,6 +2243,7 @@ erc--statusmsg-target
              ((not (eq erc-ensure-target-buffer-on-privmsg 'status)))
              ((not (erc-channel-p target)))
              (chars (erc--get-isupport-entry 'STATUSMSG 'single))
+             ((not (string-empty-p target)))
              ((string-search (string (aref target 0)) chars))
              (trimmed (substring target 1))
              ((erc-channel-p trimmed)))
@@ -2169,11 +2298,35 @@ erc--message-speaker-catalog
 (defvar erc--speaker-status-prefix-wanted-p (gensym "erc-")
   "Sentinel to detect whether `erc-format-@nick' has just run.")
 
+(erc--define-zresponse (PRIVMSG NOTICE)
+  :include erc--zstatused
+  ;; Unlike with erc--zJOIN, this init form can't ensure a buffer
+  ;; exists because an inordinate amount of conditions must be met.
+  ;; It follows that this field will be empty in buffer-init hooks,
+  ;; like `erc-connect-pre-hook' for new queries.
+  ( buffer (erc-get-buffer (if (erc--zPRIVMSG-query-p parsed)
+                               (car (erc--zresponse-nuh parsed))
+                             (erc--zstatused-target parsed))
+                           erc-server-process)
+    :type buffer)
+  ( query-p (equal (erc-downcase (erc--zstatused-target parsed))
+                   (erc--zresponse-mynick-d parsed))
+    :type boolean)
+  ( speaker (car (erc--zPRIVMSG-nuh parsed))
+    :type string)
+  ( notice-p (string= (erc-response.command parsed) "NOTICE")
+    :type boolean)
+  ( input-p (string= (erc--zPRIVMSG-mynick-d parsed)
+                     (car (erc--zPRIVMSG-nuh parsed)))
+    :type boolean))
+
 (define-erc-response-handler (PRIVMSG NOTICE)
   "Handle private messages, including messages in channels." nil
+  (unless (erc--zPRIVMSG-p parsed)
+    (setq parsed (erc--zPRIVMSG-from-parsed parsed)))
   (let ((sender-spec (erc-response.sender parsed))
         (cmd (erc-response.command parsed))
-        (tgt (car (erc-response.command-args parsed)))
+        (tgt (erc--zPRIVMSG-target parsed))
         (msg (erc-response.contents parsed)))
     (defvar erc-minibuffer-ignored)
     (defvar erc-ignore-list)
@@ -2182,24 +2335,24 @@ erc--speaker-status-prefix-wanted-p
             (and erc-ignore-reply-list (erc-ignored-reply-p msg tgt proc)))
         (when erc-minibuffer-ignored
           (message "Ignored %s from %s to %s" cmd sender-spec tgt))
-      (let* ((sndr (erc-parse-user sender-spec))
+      (let* ((sndr (erc--zPRIVMSG-nuh parsed))
              (nick (nth 0 sndr))
              (login (nth 1 sndr))
              (host (nth 2 sndr))
-             (msgp (string= cmd "PRIVMSG"))
-             (noticep (string= cmd "NOTICE"))
+             (noticep (erc--zPRIVMSG-notice-p parsed))
+             (msgp (not noticep))
              ;; S.B. downcase *both* tgt and current nick
-             (medown (erc-downcase (erc-current-nick)))
-             (inputp (string= medown (erc-downcase nick)))
-             (privp (string= (erc-downcase tgt) medown))
+             (inputp (erc--zPRIVMSG-input-p parsed))
+             (privp (erc--zPRIVMSG-query-p parsed))
              (erc--display-context `((erc-buffer-display . ,(intern cmd))
                                      ,@erc--display-context))
              (erc--msg-prop-overrides `((erc--tmp) ,@erc--msg-prop-overrides))
              (erc--speaker-status-prefix-wanted-p nil)
              (erc-current-message-catalog erc--message-speaker-catalog)
+             (statusmsg (erc--zPRIVMSG-statusmsg parsed))
              ;;
-             finalize buffer statusmsg cmem-prefix fnick)
-        (setq buffer (erc-get-buffer (if privp nick tgt) proc))
+             finalize buffer cmem-prefix fnick)
+        (setq buffer (erc--zPRIVMSG-buffer parsed))
         ;; Even worth checking for empty target here? (invalid anyway)
         (unless (or buffer noticep (string-empty-p tgt) (eq ?$ (aref tgt 0))
                     (erc-is-message-ctcp-and-not-action-p msg))
@@ -2215,15 +2368,12 @@ erc--speaker-status-prefix-wanted-p
                 (push `(erc-receive-query-display . ,(intern cmd))
                       erc--display-context)
                 (setq buffer (erc--open-target nick)))
-            (cond
-             ;; Target is a channel and contains leading @+ chars.
-             ((and-let* ((trimmed(erc--statusmsg-target tgt)))
-                (setq buffer (erc-get-buffer trimmed proc)
-                      statusmsg (and buffer (substring tgt 0 1)))))
-             ;; A channel buffer has been killed but is still joined.
-             (erc-ensure-target-buffer-on-privmsg
-              (setq buffer (erc--open-target tgt))))))
+            ;; A channel buffer has been killed but is still joined.
+            ;; Or, an "echoed" query has arrived from self to other.
+            (when erc-ensure-target-buffer-on-privmsg
+              (setq buffer (erc--open-target tgt)))))
         (when buffer
+          (setf (erc--zPRIVMSG-buffer parsed) buffer) ; may have changed
           (with-current-buffer buffer
             (when privp
               (erc--unhide-prompt)
@@ -2279,13 +2429,15 @@ erc--speaker-status-prefix-wanted-p
           (when finalize (funcall finalize)))
         nil))))
 
+(erc--define-zresponse (QUIT))
+
 (define-erc-response-handler (QUIT)
   "Another user has quit IRC." nil
   (let ((reason (erc-response.contents parsed))
         (erc--msg-prop-overrides erc--msg-prop-overrides)
         bufs)
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
+                 (erc--zQUIT-nuh parsed)))
       (setq bufs (erc-buffer-list-with-nick nick proc))
       (when (erc-current-nick-p nick)
         (setq bufs (append (erc--query-list) bufs))
@@ -2297,16 +2449,25 @@ erc--speaker-status-prefix-wanted-p
       (erc-remove-user nick)))
   nil)
 
+(erc--define-zresponse (TOPIC)
+  :include erc--ztargeted
+  ( buffer (let ((target (erc--ztargeted-target parsed)))
+             (erc-get-buffer target erc-server-process))
+    :type buffer
+    :documentation "Buffer of the channel whose topic is being set."))
+
 (define-erc-response-handler (TOPIC)
   "The channel topic has changed." nil
-  (let* ((ch (car (erc-response.command-args parsed)))
+  (let* ((ch (erc--zTOPIC-target parsed))
          (topic (erc-trim-string (erc-response.contents parsed)))
-         (time (format-time-string erc-server-timestamp-format)))
+         (time (format-time-string erc-server-timestamp-format
+                                   (erc--zTOPIC-time parsed))))
     (pcase-let ((`(,nick ,login ,host)
-                 (erc-parse-user (erc-response.sender parsed))))
-      (erc-update-channel-member ch nick nick nil nil nil nil nil nil host login)
+                 (erc--zTOPIC-nuh parsed)))
+      (erc-update-channel-member ch nick nick nil nil nil nil nil nil
+                                 host login)
       (erc-update-channel-topic ch (format "%s\C-o (%s, %s)" topic nick time))
-      (erc-display-message parsed 'notice (erc-get-buffer ch proc)
+      (erc-display-message parsed 'notice (erc--zTOPIC-buffer parsed)
                            'TOPIC ?n nick ?u login ?h host
                            ?c ch ?T topic))))
 
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index b64a2a61cd1..66c1eb40aa4 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -2618,12 +2618,13 @@ erc-message
         (erc-update-current-channel-member "tester" "tester"))
 
       (with-current-buffer "ExampleNet"
-        (erc-server-PRIVMSG erc-server-process
-                            (make-erc-response
-                             :sender "alice!~u@fsf.org"
-                             :command "PRIVMSG"
-                             :command-args '("#chan" "hi")
-                             :unparsed ":alice!~u@fsf.org PRIVMSG #chan :hi"))
+        (let ((parsed (make-erc-response
+                       :sender "alice!~u@fsf.org"
+                       :command "PRIVMSG"
+                       :command-args '("#chan" "hi")
+                       :unparsed ":alice!~u@fsf.org PRIVMSG #chan :hi")))
+          (erc-server-PRIVMSG erc-server-process
+                              (erc--zPRIVMSG-from-parsed parsed)))
         (should (equal erc-server-last-peers '("alice")))
         (should (string-match "<alice>" (pop calls))))
 
@@ -4088,6 +4089,24 @@ erc--define-lazy-accessors
                   val)))))
          (defalias 'erc-tests--my-struct-foo #'erc-tests--my-struct--foo))))))
 
+(ert-deftest erc--zPRIVMSG ()
+  (let* ((inst (make-erc--zPRIVMSG :sender "foo"))
+         (foo (list 42 inst [42])))
+    ;; GV place.
+    (should (equal '(1 "foo" "" "") (push 1 (erc--zPRIVMSG-nuh (nth 1 foo)))))
+    (should (equal (erc--zPRIVMSG-nuh inst) '(1 "foo" "" "")))
+    (should (equal (erc--zPRIVMSG-speaker inst) 1))
+
+    ;; Initialized value always reused rather than derived again.
+    (should (eql 1 (pop (erc--zPRIVMSG-nuh (nth 1 foo)))))
+    (should (eql 2 (cl-incf (erc--zPRIVMSG-speaker (nth 1 foo)))))
+    (should (equal (erc--zPRIVMSG-nuh inst) '("foo" "" "")))
+    (should (eql 2 (erc--zPRIVMSG-speaker inst)))
+
+    ;; Normal setf.
+    (should (equal (setf (car (erc--zPRIVMSG-nuh (nth 1 foo))) "bar") "bar"))
+    (should (equal (erc--zPRIVMSG-nuh inst) '("bar" "" "")))))
+
 (ert-deftest erc-tests-common-string-to-propertized-parts ()
   :tags '(:unstable) ; only run this locally
   (unless (>= emacs-major-version 28) (ert-skip "Missing `object-intervals'"))
-- 
2.48.1

