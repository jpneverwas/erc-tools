From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sat, 21 Aug 2021 02:13:10 -0700
Subject: [PATCH 34/37] [POC] Add a couple v3 demo modules for ERC

Add temporary demo module `names' demonstrating incremental,
server-initiated updates of user data (state syncing for users with whom
we share a common channel).  Also add temporary demo module `eldoc'
providing working demo of various `v3' features based around these live
status updates (away, account, etc.). Hovering over a nick shows current
status summary in echo area.

* lisp/erc/erc-eldoc.el: New file.
* lisp/erc/erc-names.el: New file.
* test/lisp/erc/erc-eldoc-tests.el: New file.
* test/lisp/erc/erc-scenarios-names.el: New file.
---
 lisp/erc/erc-eldoc.el                |  99 +++++++++++
 lisp/erc/erc-names.el                | 257 +++++++++++++++++++++++++++
 test/lisp/erc/erc-eldoc-tests.el     |  81 +++++++++
 test/lisp/erc/erc-scenarios-names.el |  73 ++++++++
 4 files changed, 510 insertions(+)
 create mode 100644 lisp/erc/erc-eldoc.el
 create mode 100644 lisp/erc/erc-names.el
 create mode 100644 test/lisp/erc/erc-eldoc-tests.el
 create mode 100644 test/lisp/erc/erc-scenarios-names.el

diff --git a/lisp/erc/erc-eldoc.el b/lisp/erc/erc-eldoc.el
new file mode 100644
index 00000000000..39c775a9a5e
--- /dev/null
+++ b/lisp/erc/erc-eldoc.el
@@ -0,0 +1,99 @@
+;;; erc-eldoc.el --- eldoc integration for ERC -*- lexical-binding: t; -*-
+
+;; Copyright (C) 2021 Free Software Foundation, Inc.
+;;
+;; This file is part of GNU Emacs.
+;;
+;; GNU Emacs is free software: you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published
+;; by the Free Software Foundation, either version 3 of the License,
+;; or (at your option) any later version.
+;;
+;; GNU Emacs is distributed in the hope that it will be useful, but
+;; WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+;; General Public License for more details.
+;;
+;; You should have received a copy of the GNU General Public License
+;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
+
+;;; Commentary:
+
+;; TODO delete this and/or replace with something nice.
+
+;; This is a throwaway module meant to demo some IRCv3 features like
+;; live account awareness and live away status.
+
+;;; Code:
+
+(require 'erc-v3)
+(require 'erc-button)
+
+;; Can't use `erc-format-@nick' because `erc-get-user-mode-prefix'
+;; relies on global hash table which may no longer contain the entry
+;; we want
+
+(defun erc-eldoc-nick-at-point (callback &rest _ignored)
+  "Document nick at point.
+When point contains a nick added by `erc-button-add-nickname-buttons' or
+a name in a 353 user list, call CALLBACK.  For non-eldoc use, see
+`erc-nick-at-point'."
+  (when-let* ((data (get-text-property (point) 'erc-data))
+              (server-user (nth 1 data))
+              ((or (eq (get-text-property (point) 'erc-callback)
+                       'erc-button--perform-nick-popup)
+                   (pcase (get-text-property (point) 'font-lock-face)
+                     ((and (pred consp) v)
+                      (memq 'erc-notice-face (flatten-list v)))
+                     ('erc-notice-face t)))))
+    (let* ((cuser (nth 2 data))
+           (statusp (not (zerop (erc-channel-user-status cuser))))
+           (account (or (erc-server-user-account server-user) "*"))
+           (nick (erc-server-user-nickname server-user))
+           (partedp (not (erc-get-channel-user nick)))
+           (pfx (if statusp (concat (erc-v3--format-prefix cuser t) " ") ""))
+           (info (erc-v3--assemble-server-user-info-string server-user))
+           (noti-fa (if partedp 'erc-error-face 'erc-notice-face)))
+      (when pfx
+        (put-text-property 0 (length pfx) 'face
+                           (if partedp 'erc-error-face 'erc-nick-prefix-face)
+                           pfx))
+      (put-text-property 0 (length info) 'face noti-fa
+                         (if (string-empty-p info) "?" info))
+      (funcall callback (concat pfx info)
+               :thing account
+               :face noti-fa))))
+
+;;;###autoload(autoload 'erc-eldoc-mode "erc-eldoc" nil t)
+(define-erc-module eldoc nil
+  "Toy eldoc mode integration demoing some IRCv3 features for ERC.
+To use, add the symbol `eldoc' to `erc-modules' or run M-x
+erc-eldoc-mode RET in some target buffer."
+  ((require 'eldoc)
+   (eldoc-add-command 'erc-button-previous)
+   (eldoc-add-command 'erc-button-next)
+   (when erc--target
+     (add-hook 'eldoc-documentation-functions #'erc-eldoc-nick-at-point nil t)
+     (eldoc-mode +1)))
+  ((eldoc-remove-command 'erc-button-previous)
+   (eldoc-remove-command 'erc-button-next)
+   (when erc--target
+     (remove-hook 'eldoc-documentation-functions #'erc-eldoc-nick-at-point t)
+     (eldoc-mode -1)))
+  localp)
+
+;; FIXME use or lose (maybe `erc-once-with-server-event' on 329, dunno)
+
+(defun erc-eldoc-maybe-whine-about-requirements ()
+  "Maybe print notice saying requirements unsatisfied."
+  (erc-with-server-buffer
+    (unless (< 1 (seq-count #'identity (list erc-v3--extended-join
+                                             erc-v3--account-notify
+                                             erc-v3--whox
+                                             erc-v3--account-tag)))
+      (erc-display-error-notice
+       nil "Required IRCv3 support for eldoc module not found."))))
+
+(provide 'erc-eldoc)
+
+;;; erc-eldoc.el ends here
diff --git a/lisp/erc/erc-names.el b/lisp/erc/erc-names.el
new file mode 100644
index 00000000000..e78fe6accb7
--- /dev/null
+++ b/lisp/erc/erc-names.el
@@ -0,0 +1,257 @@
+;;; erc-names.el --- Tabular /NAMES for ERC -*- lexical-binding: t; -*-
+
+;; Copyright (C) 2021 Free Software Foundation, Inc.
+;;
+;; This file is part of GNU Emacs.
+;;
+;; GNU Emacs is free software: you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published
+;; by the Free Software Foundation, either version 3 of the License,
+;; or (at your option) any later version.
+;;
+;; GNU Emacs is distributed in the hope that it will be useful, but
+;; WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+;; General Public License for more details.
+;;
+;; You should have received a copy of the GNU General Public License
+;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
+
+;;; Commentary:
+
+;; This is currently a throwaway module meant to demonstrate how
+;; incremental user-state updates can enrich the user experience.  It
+;; replaces the /NAMES command for refreshing a channel's manifest via
+;; I/O with one that merely displays the current, rolling local state
+;; in a `tabulated-list' buffer.
+;;
+;; TODO use vtable instead of tabulated-list if available
+;; TODO delete or replace with something nice.
+
+;;; Code:
+(require 'erc-v3)
+(require 'erc-button)
+(require 'tabulated-list)
+(require 'text-property-search)
+
+(defcustom erc-names-columns '(15 15 25 20 5 5 20 12)
+  "Columns for tabulated list of channel members."
+  :group 'erc
+  :type '(list integer integer integer integer
+               integer integer integer integer))
+
+(defvar erc-names--channel-buffer nil
+  "The channel buffer associated with the tabulated list buffer.")
+
+(defvar-local erc-names-buffer nil
+  "The names buffer for the current channel, if any.")
+
+(defun erc-names--string-less-p (n a b)
+  (string< (aref (cadr a) n) (aref (cadr b) n)))
+
+(defun erc-names--status-less-p (a b)
+  (< (let ((s (aref (cadr a) 4)))
+       (if (string= s "") 5 (seq-position "qaohv" (aref s 0))))
+     (let ((s (aref (cadr b) 4)))
+       (if (string= s "") 5 (seq-position "qaohv" (aref s 0))))))
+
+(defun erc-names--perform-action ()
+  (interactive)
+  (when-let* ((entry (tabulated-list-get-entry))
+              (name (aref entry 0)))
+    (with-current-buffer erc-names--channel-buffer
+      (unless (erc-nick-equal-p name (erc-current-nick))
+        (erc-nick-popup name)))))
+
+(defvar-keymap erc-names-major-mode-map
+  :doc "Local keymap for `erc-names-major-mode-map' buffers."
+  :parent tabulated-list-mode-map
+  "RET" #'erc-names--perform-action)
+
+(define-derived-mode erc-names-major-mode
+  tabulated-list-mode "ERC Channel Names"
+  "Major mode for listing channel members."
+  :interactive nil
+  (setq tabulated-list-format
+        `[("Nick"
+           ,(nth 0 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 0))
+          ("Account"
+           ,(nth 1 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 1))
+          ("Userhost"
+           ,(nth 2 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 2))
+          ("Full Name"
+           ,(nth 3 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 3))
+          ("Status"
+           ,(nth 4 erc-names-columns)
+           erc-names--status-less-p)
+          ("Away"
+           ,(nth 5 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 5))
+          ("Away Msg"
+           ,(nth 6 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 6))
+          ("Buffers"
+           ,(nth 7 erc-names-columns)
+           ,(apply-partially #'erc-names--string-less-p 7))])
+  (setq tabulated-list-padding 2
+        tabulated-list-sort-key (cons "Status" nil)
+        revert-buffer-function #'erc-names-list-refresh)
+  (tabulated-list-init-header))
+
+(defun erc-names--create-entry (cusr)
+  "Assemble a row object from `erc-channel-members' value CUSR."
+  (let* ((user (car cusr))
+         (awayp (erc--server-user-away-p user)))
+    (vector
+     ;; 0 Nick
+     (erc-server-user-nickname user)
+     ;; 1 Account
+     (or (erc-server-user-account user) "*")
+     ;; 2 Userhost
+     (if-let* ((login (erc-server-user-login user))
+               (host (erc-server-user-host user)))
+         (concat login "@" host)
+       "")
+     ;; 3 Full Name
+     (if-let* ((full-name (erc-server-user-full-name user))
+               ((not (string-empty-p full-name))))
+         full-name
+       "")
+     ;; 4 Status
+     (or (erc-v3--format-prefix (cdr cusr) t) "")
+     ;; 5 Away
+     (if awayp "G" "H")
+     ;; 6 Away Msg
+     (or awayp "")
+     ;; 7 Buffers
+     (if-let* ((bufs (erc-server-user-buffers user)))
+         (string-join (sort (delete (erc-target) (mapcar #'buffer-name bufs))
+                            #'string<)
+                      ",")
+       ""))))
+
+;; FIXME remove this or use button cache instead.  Users shouldn't
+;; expect this to work when disconnected anyway.
+(defun erc-names--refill ()
+  (let ((table (make-hash-table :test #'equal)))
+    (save-excursion
+      (save-restriction
+        (widen)
+        (goto-char (point-min))
+        (while (when-let*
+                   ((found (text-property-search-forward 'erc-data))
+                    (key (prop-match-value found))
+                    (user (nth 1 key))
+                    (beg (prop-match-beginning found))
+                    (chl (or (nth 2 key) (make-erc-channel-user)))
+                    (nick (erc-server-user-nickname user)))
+                 (puthash nick (cons user chl) table)))))
+    table))
+
+(defun erc-names--refresh-contents (&optional _ignore-auto _no-confirm)
+  (setq tabulated-list-entries
+        (let ((id (tabulated-list-get-id))
+              (entries ()))
+          (with-current-buffer erc-names--channel-buffer
+            (maphash (lambda (_ u)
+                       (push (list id (erc-names--create-entry u)) entries))
+                     (if (zerop (hash-table-count erc-channel-users))
+                         (erc-names--refill)
+                       erc-channel-users)))
+          entries)))
+
+(defun erc-names-list-refresh (&rest _)
+  (erc-names--refresh-contents)
+  (tabulated-list-print t))
+
+(defun erc-names-list (channel)
+  (interactive (list (erc-target)))
+  (with-current-buffer (erc-get-buffer channel erc-server-process)
+    (let ((channel-buffer (current-buffer))
+          (buf (get-buffer-create (format "*%s/NAMES*" (buffer-name)))))
+      (with-current-buffer (or (and (buffer-live-p erc-names-buffer)
+                                    erc-names-buffer)
+                               (setq erc-names-buffer buf))
+        (erc-names-major-mode)
+        (setq erc-names--channel-buffer channel-buffer)
+        (erc-names--refresh-contents)
+        (tabulated-list-print)
+        (pop-to-buffer buf)))))
+
+(define-erc-module names nil
+  "Module providing /NAMES replacement for ERC."
+  ((advice-add 'erc-cmd-NAMES :around #'erc-names--cmd))
+  ((put 'erc-cmd-NAMES 'process-not-needed t)
+   (advice-remove 'erc-cmd-NAMES #'erc-names-list)
+   (put 'erc-cmd-NAMES 'process-not-needed nil)))
+
+(defun erc-names--cmd (orig &rest args)
+  "Call `erc-names-list' if `erc-names-mode' is enabled, else ORIG with ARGS."
+  (if (and erc-v3-mode erc-names-mode)
+      (always (erc-names-list (or (car args) (erc-target))))
+    (funcall orig)))
+
+
+;;;; Extension: no-implicit-names
+
+;; XXX this currently does nothing.
+
+;; TODO only enable when reconnecting within some maximum allowed time
+;; frame, and persist phantom cmems or equivalent from button store.
+
+;;;###autoload(put 'draft/no-implicit-names 'erc--feature 'erc-names)
+;;;###autoload(put 'no-implicit-names 'erc--feature 'erc-names)
+(erc-v3--define-capability no-implicit-names
+  "Update server users from \"WHOX\" instead of \"NAMES\".
+Print a one-line summary instead of listing all names."
+  :aliases '(draft/no-implicit-names)
+  (if erc-v3--no-implicit-names
+      (if erc--target
+          (progn
+            (unless erc-names-mode
+              (erc-names-mode +1))
+            (add-hook 'erc-v3--whox-on-354-functions
+                      #'erc-names--increment-tally 80 t))
+        (add-hook 'erc-server-315-functions #'erc-names--bake-tally 80 t))
+    (remove-hook 'erc-v3--whox-on-354-functions #'erc-names--increment-tally t)
+    (remove-hook 'erc-server-315-functions #'erc-names--bake-tally t)
+    (kill-local-variable 'erc-names--tally)))
+
+(defvar erc-message-english-names-summary "Users on %c (%d)"
+  "English `erc-format-message' template for key `names-summary'.")
+
+(defvar-local erc-names--tally nil)
+
+(defun erc-names--increment-tally ()
+  (cl-assert erc-names--tally)
+  (setcar erc-names--tally
+          (number-to-string (hash-table-count erc-channel-members))))
+
+(defun erc-names--bake-tally (_ parsed)
+  (let ((target (nth 1 (erc-response.command-args parsed))))
+    (erc-with-buffer (target)
+      (erc-v3--bake-display-count (point-min) erc-insert-marker
+                                  erc-names--tally))))
+
+(cl-defmethod erc-v3--whox-on-join
+  (&context (erc-v3--no-implicit-names erc-v3--no-implicit-names)
+            (erc-v3--whox erc-v3--whox))
+  "Insert an informative local notice."
+  (cl-call-next-method)
+  (setq erc-names--tally
+        (list (number-to-string (hash-table-count erc-channel-members))))
+  (erc-display-message nil 'notice (current-buffer)
+                       'names-summary ?c (erc-target)
+                       ?d (propertize "   " 'display erc-names--tally)))
+
+(provide 'erc-names)
+
+;;; erc-names.el ends here
+
+;; Local Variables:
+;; generated-autoload-file: "erc-loaddefs.el"
+;; End:
diff --git a/test/lisp/erc/erc-eldoc-tests.el b/test/lisp/erc/erc-eldoc-tests.el
new file mode 100644
index 00000000000..ab0cc09e93e
--- /dev/null
+++ b/test/lisp/erc/erc-eldoc-tests.el
@@ -0,0 +1,81 @@
+;;; erc-eldoc-tests.el --- Tests for erc-eldoc.  -*- lexical-binding:t -*-
+
+;; Copyright (C) 2020-2022 Free Software Foundation, Inc.
+;;
+;; This file is part of GNU Emacs.
+;;
+;; GNU Emacs is free software: you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published by
+;; the Free Software Foundation, either version 3 of the License, or
+;; (at your option) any later version.
+;;
+;; GNU Emacs is distributed in the hope that it will be useful,
+;; but WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+;; GNU General Public License for more details.
+;;
+;; You should have received a copy of the GNU General Public License
+;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
+
+;;; Commentary:
+
+;; TODO: This file is just a demo.  Rewrite or delete.
+
+;;; Code:
+
+(require 'ert-x)
+(eval-and-compile
+  (let ((load-path (cons (ert-resource-directory) load-path)))
+    (require 'erc-tests-common)))
+
+(require 'erc-eldoc)
+(require 'erc-button)
+
+(ert-deftest erc-eldoc-nick-at-point ()
+  (let ((erc-insert-modify-hook erc-insert-modify-hook)
+        ;;
+        user
+        erc-kill-channel-hook erc-kill-server-hook erc-kill-buffer-hook)
+    (cl-pushnew #'erc-button-add-buttons erc-insert-modify-hook)
+
+    (erc-tests-common-make-server-buf (buffer-name))
+    (setq erc-server-users (make-hash-table :test 'equal))
+    (erc-button--initialize-users)
+    (setq user (make-erc-server-user
+                :nickname "Bob"
+                :account "bobzilla"
+                :full-name "Bob Zilla"
+                :host "localhost"
+                :login "~bob"))
+    (erc-button--ensure-data-prop "bob" user nil)
+    (puthash "bob" user erc-server-users)
+
+    (with-current-buffer (erc--open-target "#chan")
+      (erc-button--initialize-users)
+      (puthash "bob" (cons user (make-erc-channel-user :voice t))
+               erc-channel-members)
+      (let ((msg (erc-format-privmessage "Bob" "hi" nil t)))
+        (erc-display-message nil nil (current-buffer) msg)
+        (goto-char (point-min))
+        (should (search-forward "<B" nil t))
+        (forward-char)
+        (should (equal (erc-eldoc-nick-at-point #'list)
+                       '(#("v ~bob@localhost \"Bob Zilla\""
+                           0 1 (help-echo "voice" face erc-nick-prefix-face)
+                           1 2 (face erc-nick-prefix-face)
+                           2 28 (face erc-notice-face))
+                         :thing "bobzilla" :face erc-notice-face)))
+
+        ;; Removing user changes face.
+        (remhash "bob" erc-channel-users)
+        (should (equal (erc-eldoc-nick-at-point #'list)
+                       '(#("v ~bob@localhost \"Bob Zilla\""
+                           0 1 (help-echo "voice" face erc-nick-prefix-face)
+                           1 2 (face erc-nick-prefix-face)
+                           2 28 (face erc-error-face))
+                         :thing "bobzilla" :face erc-error-face)))))
+
+    (when noninteractive
+      (kill-buffer "#chan"))))
+
+;;; erc-eldoc-tests.el ends here
diff --git a/test/lisp/erc/erc-scenarios-names.el b/test/lisp/erc/erc-scenarios-names.el
new file mode 100644
index 00000000000..a45ea095b64
--- /dev/null
+++ b/test/lisp/erc/erc-scenarios-names.el
@@ -0,0 +1,73 @@
+;;; erc-scenarios-names.el --- erc-names-mode -*- lexical-binding: t -*-
+
+;; Copyright (C) 2025 Free Software Foundation, Inc.
+
+;; This file is part of GNU Emacs.
+
+;; GNU Emacs is free software: you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published by
+;; the Free Software Foundation, either version 3 of the License, or
+;; (at your option) any later version.
+
+;; GNU Emacs is distributed in the hope that it will be useful,
+;; but WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+;; GNU General Public License for more details.
+
+;; You should have received a copy of the GNU General Public License
+;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
+
+;;; Code:
+
+(require 'ert-x)
+(eval-and-compile
+  (let ((load-path (cons (ert-resource-directory) load-path)))
+    (require 'erc-scenarios-common)))
+
+(require 'erc-names)
+
+(ert-deftest erc-scenarios-names ()
+  :tags '(:expensive-test)
+  (erc-scenarios-common-with-cleanup
+      ((erc-scenarios-common-dialog "v3/userhost-in-names")
+       (erc-server-flood-penalty 0.1)
+       (dumb-server (erc-d-run "localhost" t 'basic))
+       (port (process-contact dumb-server :service))
+       (dumb-server-buffer (get-buffer "*erc-d-server*"))
+       (erc-modules `(v3 names ,@erc-modules))
+       (erc-autojoin-channels-alist '((ExampleOrg "#chan" "#spam")))
+       (expect (erc-d-t-make-expecter)))
+
+    (ert-info ("Connect")
+      (with-current-buffer (erc :server "127.0.0.1"
+                                :port port
+                                :nick "tester"
+                                :password "password123"
+                                :full-name "tester")
+        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))
+
+    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
+      (funcall expect 10 "#chan was created on"))
+
+    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#spam"))
+      (funcall expect 10 "#spam was created on")
+      (erc-scenarios-common-say "/names"))
+
+    (with-current-buffer "*#spam/NAMES*"
+      (funcall expect 1 "mike")
+      (funcall expect 1 "joe")
+      (funcall expect 1 "SHA")
+      (funcall expect 1 "focused-ne")
+      (funcall expect 1 "RELAX")
+      (funcall expect 1 "gallant")
+      (funcall expect 1 "ecstatic")
+
+      ;; Simulate pressing a key over some entry to PM them.
+      (let ((inhibit-interaction nil))
+        (ert-simulate-keys "Query\r"
+          (erc-names--perform-action)))
+      (should (get-buffer "ecstatic"))
+
+      (funcall expect 1 "TRUST"))))
+
+;;; erc-scenarios-names.el ends here
-- 
2.48.1

