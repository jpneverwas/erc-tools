From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Wed, 7 Feb 2024 05:03:55 -0800
Subject: [PATCH 16/37] [5.x] Replace erc--cmem-from-nick-function args with
 struct

* lisp/erc/erc-backend.el (erc-server-PRIVMSG): Create
`erc--msg-parts' object as input for `erc--cmem-from-nick-function'.
Assign contents of `msg' slot to static `msg' variable after the
latter runs.
* lisp/erc/erc-button.el (erc-button--add-phantom-speaker): Update to
expect `erc--msg-parts' object.
* lisp/erc/erc-common.el (erc--nonempty-str): New helper function.
(erc--msg-parts): New struct.
* lisp/erc/erc.el (erc--cmem-from-nick-function): Replace loose
parameters with single struct.
(erc--cmem-get-existing): Update function to expect `erc--msg-parts'
object.
---
 lisp/erc/erc-backend.el |  9 +++++----
 lisp/erc/erc-button.el  |  8 ++++----
 lisp/erc/erc-common.el  | 20 ++++++++++++++++++++
 lisp/erc/erc.el         | 20 +++++++++++---------
 4 files changed, 40 insertions(+), 17 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index e48c7845ce1..52d8b4c2568 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -2194,14 +2194,15 @@ erc--speaker-status-prefix-wanted-p
             (defvar erc-format-nick-function)
             (defvar erc-show-speaker-membership-status)
             (defvar erc-speaker-from-channel-member-function)
-            (let ((cdata (funcall erc--cmem-from-nick-function
-                                  (erc-downcase nick) sndr parsed)))
+            (let* ((parts (erc--msg-parts-from-nuh :nuh sndr :parsed parsed))
+                   (cmem (funcall erc--cmem-from-nick-function parts)))
               (setq fnick (funcall erc-speaker-from-channel-member-function
-                                   (car cdata) (cdr cdata))
+                                   (car cmem) (cdr cmem))
+                    msg (erc--msg-parts-msg parts)
                     cmem-prefix (and (or erc--speaker-status-prefix-wanted-p
                                          erc-show-speaker-membership-status
                                          inputp)
-                                     (cdr cdata))))))
+                                     (cdr cmem))))))
         (if (erc-is-message-ctcp-p msg)
             (if noticep
                 (erc-process-ctcp-reply proc parsed nick login host
diff --git a/lisp/erc/erc-button.el b/lisp/erc/erc-button.el
index ec5a012b8c9..306ffd59e1e 100644
--- a/lisp/erc/erc-button.el
+++ b/lisp/erc/erc-button.el
@@ -415,9 +415,9 @@ erc-button--get-user-from-spkr-prop
 (cl-defstruct (erc--phantom-channel-user (:include erc-channel-user)))
 (cl-defstruct (erc--phantom-server-user (:include erc-server-user)))
 
-(defun erc-button--add-phantom-speaker (downcased nuh _parsed)
-  (pcase-let* ((`(,nick ,login ,host) nuh)
-               (cmem (gethash downcased erc-button--phantom-cmems))
+(defun erc-button--add-phantom-speaker (parts)
+  (pcase-let* (((cl-struct erc--msg-parts nick login host key) parts)
+               (cmem (gethash key erc-button--phantom-cmems))
                (user (or (car cmem)
                          (make-erc--phantom-server-user
                           :nickname nick
@@ -426,7 +426,7 @@ erc-button--add-phantom-speaker
                (cuser (or (cdr cmem)
                           (make-erc--phantom-channel-user
                            :last-message-time (current-time)))))
-    (puthash downcased (cons user cuser) erc-button--phantom-cmems)
+    (puthash key (cons user cuser) erc-button--phantom-cmems)
     (cons user cuser)))
 
 (defun erc-button--get-phantom-cmem (down _word _bounds _count)
diff --git a/lisp/erc/erc-common.el b/lisp/erc/erc-common.el
index 46c19e78fca..a973750efef 100644
--- a/lisp/erc/erc-common.el
+++ b/lisp/erc/erc-common.el
@@ -169,6 +169,26 @@ erc--isupport-data
   (table (make-char-table 'erc--channel-mode-types) :type char-table)
   (shortargs (make-hash-table :test #'equal)))
 
+(cl-defstruct (erc--msg-parts
+               (:constructor erc--msg-parts-from-nuh
+                             (&key nuh parsed &aux
+                                   (nick (nth 0 nuh))
+                                   (msg (erc-response.contents parsed))
+                                   (login (erc--nonempty-str (nth 1 nuh)))
+                                   (host (erc--nonempty-str (nth 2 nuh)))
+                                   (key (erc-downcase nick)))))
+  (key "" :type string :documentation "Downcased nick.")
+  (msg "" :type string :documentation "Message body.")
+  (nick "" :type string)
+  (login nil :type (or null string))
+  (host nil :type (or null string))
+  (parsed (make-erc-response) :type erc-response))
+
+(define-inline erc--nonempty-str (string)
+  "Evaluate and return nonempty STRING if it's well and truly thus."
+  (inline-letevals (string)
+    (inline-quote (and ,string (not (string-empty-p ,string)) ,string))))
+
 ;; After dropping 28, we can use prefixed "erc-autoload" cookies.
 (defun erc--normalize-module-symbol (symbol)
   "Return preferred SYMBOL for `erc--module'."
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index 95326eb47c5..eebae15a681 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -6321,15 +6321,17 @@ erc--get-speaker-bounds
     (cons beg (next-single-property-change beg 'erc--speaker))))
 
 (defvar erc--cmem-from-nick-function #'erc--cmem-get-existing
-  "Function maybe returning a \"channel member\" cons from a nick.
-Must return either nil or a cons of an `erc-server-user' and an
-`erc-channel-user' (see `erc-channel-users') for use in
-formatting a user's nick prior to insertion.  Called in the
-appropriate target buffer with the downcased nick, the parsed
-NUH, and the current `erc-response' object.")
-
-(defun erc--cmem-get-existing (downcased _nuh _parsed)
-  (and erc-channel-users (gethash downcased erc-channel-users)))
+  "Function returning a \"channel member\" cons cell.
+Called with an `erc--msg-parts' object derived from sender info.  Must
+return either nil or a \"cmem\" cons cell consisting of an
+`erc-server-user' and an `erc-channel-user' (see `erc-channel-members')
+for use in formatting a nick for insertion.  Runs after the sender's
+`erc-channel-members' entry has been created or updated.")
+
+(defun erc--cmem-get-existing (parts)
+  "Return `erc-channel-members' entry from `erc--msg-parts' PARTS."
+  (and erc-channel-members
+       (gethash (erc--msg-parts-key parts) erc-channel-members)))
 
 (defun erc-format-privmessage (nick msg privp msgp)
   "Format a PRIVMSG in an insertable fashion.
-- 
2.48.1

