(require 'erc)

(cl-macrolet ((at (d &rest b) `(run-at-time ,d nil (lambda () (progn ,@b)))))

  (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                  :port 6667
                                  :nick "tester"
                                  :password "changeme"
                                  :full-name "tester"))

  (at 6 (execute-kbd-macro "/join #chan"))
  (at 7 (execute-kbd-macro "\r"))

  (at 11 (pop-to-buffer-same-window (erc :server "127.0.0.1"
                                         :port 6668
                                         :nick "tester"
                                         :password "changeme"
                                         :full-name "tester")))

  (at 15 (execute-kbd-macro "/join #chan"))
  (at 16 (execute-kbd-macro "\r")))
