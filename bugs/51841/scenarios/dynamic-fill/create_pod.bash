#!/bin/bash
set -e

# We're in project root
[[ -f .gitlab-ci.yml ]]

bash "scripts/create_pod.bash" create_pod
bash "scripts/create_pod.bash" create_oragono foo 6667
bash "scripts/create_pod.bash" create_ircbot alice 6667 1/2 chan
bash "scripts/create_pod.bash" create_ircbot bob 6667 2/2 chan
