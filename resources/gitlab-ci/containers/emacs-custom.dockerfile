FROM docker.io/library/debian:bullseye as base

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        xz-utils curl ca-certificates \
        libc-dev gcc g++ make autoconf automake libncurses-dev gnutls-dev \
        libdbus-1-dev libacl1-dev acl git texinfo gdb inotify-tools \
        install-info imagemagick lzip zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN git clone https://git.savannah.gnu.org/git/emacs/elpa.git /elpa && \
    git -C /elpa branch elpa-admin origin/elpa-admin && \
    git clone --depth 1 https://git.savannah.gnu.org/git/emacs.git /checkout && \
    cd /checkout && \
    git --no-pager log -n1 && \
    ./autogen.sh autoconf

CMD ["emacs"]

FROM base as standard

RUN cd /checkout && \
    ./configure && \
    make && \
    make install

FROM base as native-comp

RUN apt-get update && \
    apt-get install -y --no-install-recommends -o=Dpkg::Use-Pty=0 \
      libgccjit-10-dev zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*

RUN cd /checkout && \
    ./configure --with-native-compilation && \
    make bootstrap -j2 && \
    make install

# vim:ft=dockerfile

