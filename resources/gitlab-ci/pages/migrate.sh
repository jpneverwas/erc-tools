#!/bin/sh

set -e -x

if test -z "$CI_PROJECT_URL"; then
    CI_PROJECT_URL=https://gitlab.com/emacs-erc/bugs
fi

jq -r '.[] | select(.job_id) | .job_id' | \
    while read -r job_id; do
    # Should expand to https://gitlab.com/emacs-erc/bugs/-/jobs/1785813950/artifacts/download
    curl --silent --show-error --fail --location --output "public/$job_id.zip" \
         "$CI_PROJECT_URL/-/jobs/$job_id/artifacts/download"
    (
        cd public
        unzip "$job_id.zip"
        rm -f "$job_id.zip"
        test -d logs
        find logs -name replay.html -exec \
            sed -i \
                -e 's|https://gitlab.com/jpneverwas/erc-tools/-/jobs/1785813950/artifacts/raw/||g' \
                -e 's|jpneverwas.gitlab.io/erc-tools|emacs-erc.gitlab.io/bugs|g' \
                -e 's|gitlab.com/jpneverwas/erc-tools|gitlab.com/emacs-erc/bugs|g' \
                \{\} \;
    )
    done
