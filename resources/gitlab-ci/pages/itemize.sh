#!/bin/sh
set -e

# Alpine has busybox realpath
thisdir=$(dirname "$(realpath "$0")")
test -e grouped.json
test ! -e jobs_ids.json

for good_branch in $(jq -r 'to_entries | add | .key' < grouped.json); do
    sh "$thisdir/get_job_id.sh" "$good_branch" >> job_ids.json
done

# Items appearing later on the command line shadow those that come before
# Inject a subs: [] for now to force a placeholder widget
jq -s 'reduce .[] as $item ({}; . * $item) | to_entries | [
  .[] | select(.value.subs or .value.logs)
  | (if (.value.subs) then . else {key: .key, value: (.value + {subs: []})} end)
] | from_entries' grouped.json job_ids.json bug_links.json
