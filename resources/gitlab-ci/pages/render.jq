
# Blur eyes to see HTML
to_entries

| .[] | [
  "<div id=\"bug-\(.key)\" class=\"card m-1 pe-2 bg-danger border-0\">",
  " <ul class=\"list-group h-100 p-2 rounded bg-light\">",
  "  <div class=\"d-flex w-100 mb-1 justify-content-between\">",
  "   <h5 class=\"me-3\">",
  if (.value.subs | length) > 0 then
  "   " + .key
  else
  "    <a class=\"text-secondary text-decoration-none\"\n"
+ "       data-path=\"\(.key)/replay.html\"\n"
+ (if (.value.job_id) then
  "       href=\"logs/\(.key)/replay.html\">\n"
  else
  "       href=\"@PROJ@/\(.key)/bugs/\(.key)/patches\">\n"
  end)
+ "    \(.key)\n"
+ "    </a>\n"
  end,
  "   </h5>",
  "   <div>\n"
+ "    <small title=\"\(.value.date_full)\" class=\"date\">\(.value.date)</small>\n"
+ (if .value.logs != null then
  "    <a class=\"text-light ms-1\" role=\"img\" title=\"Download logs\"\n"
+ "       href=\"\(.value.logs)\">\n"
+ "     <svg class=\"gl-icon\">\n"
+ "      <use href=\"@PAGES@/icons.svg#log\"></use>\n"
+ "     </svg>\n"
+ "    </a>\n"
+ "    <a class=\"text-light\" role=\"img\" title=\"Download patches\"\n"
+ "       href=\"\(.value.patches)\">\n"
+ "     <svg class=\"gl-icon\">\n"
+ "      <use href=\"@PAGES@/icons.svg#file-tree\"></use>\n"
+ "     </svg>\n"
+ "    </a>\n" else ""
  end)
+ (if .value.elpa != null and .value.elpa != "" then
  "    <a class=\"text-light ms-1\" role=\"img\" title=\"Visit ELPA page\"\n"
+ "       href=\"\(.value.elpa)\">\n"
+ "     <svg class=\"gl-icon\">\n"
+ "      <use href=\"@PAGES@/icons.svg#package\"></use>\n"
+ "     </svg>\n"
+ "    </a>\n" else ""
  end)
+ "   </div>",
  "  </div>"
]

+ (
  .key as $key | .value as $value | .value.subs | map(
  "   <li class=\"list-group-item \">\n"
+ "    <a class=\"text-secondary\" data-path=\"\($key)/\(.)/replay.html\"\n"
+ "       href=\"logs/\($key)/\(.)/replay.html\">\n"
+ "    \(.)\n"
+ "    </a>\n"
+ "   </li>"
  )
)

+ ["  </ul>", "</div>"] | join("\n")


