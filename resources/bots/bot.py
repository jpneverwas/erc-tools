import irc3


@irc3.plugin
class Plugin:
    requires = ["irc3.plugins.core", "irc3.plugins.human"]

    def __init__(self, bot):
        self.bot = bot

    @irc3.event(irc3.rfc.JOIN)
    def on_join(self, mask, channel, **kw):
        """Say hi whenever someone joins"""
        if mask.nick != self.bot.nick:
            self.bot.privmsg(channel, "%s, welcome!" % mask.nick)
            return
        self.bot.channel = channel
        self.bot.privmsg(channel, "Hi!")

    @irc3.event(irc3.rfc.PRIVMSG)
    def on_msg(self, mask, data=None, **kw):
        """Log anything said"""
        self.bot.log.info("%s: %s", mask.nick, data)
