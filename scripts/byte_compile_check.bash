#!/bin/bash
set -e
set -x
thisfile=$(realpath "${BASH_SOURCE[0]}")
thisdir=${thisfile%/*}

cd /checkout

declare -a files
find lisp/erc test/lisp/erc -type f -name \*.elc -delete

echo "Checking test/lisp/erc (non-fatal) ..."
readarray -t files < <(find test/lisp/erc -type f -name \*.el)
src/emacs -Q -batch \
          -l "$thisdir/byte-compile-check.el" \
          -f byte-compile-check "${files[@]}" || :

echo "Checking lisp/erc ..."
readarray -t files < <(find lisp/erc -type f -name \*.el)
src/emacs -Q -batch \
          -l "$thisdir/byte-compile-check.el" \
          -f byte-compile-check "${files[@]}"
