#!/bin/bash
set -e
set -x

# Useless unless caller sets this.
[[ $SELECTOR ]]

cd /checkout

find lisp/erc test/lisp/erc -type f -name \*.elc -delete

echo "Checking test/lisp/misc ..."
make -C test check-misc || :
readarray -t logs < <(find /checkout/test/misc/ -name \*.log)
(( ${#logs} ))
! grep '^[^[:blank:]]\+: erc\b' "${logs[@]}"
