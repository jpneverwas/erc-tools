#!/bin/bash

# This script creates modified package tarballs as well as "intermediate" stub
# files meant for further processing by ERC's own bug-specific ELPA.

# Note that some of this script may be obsolete because it was originally
# focused on ensuring a snapshot version always presented as "newer" than that
# on HEAD. This focus involved the addition of a serial number to the
# generated snapshot version number but is no longer relevant because the
# version ERC uses for development now includes a trailing "-git" suffix. This
# script now depends on the ELPA admin script generating a version like:
#
#   5.6.1snapshot49860.0.20241024.12345
#

set -e
set -x
thisfile=$(realpath "${BASH_SOURCE[0]}")
thisdir=${thisfile%/*}
rootdir=$(realpath "$thisdir/..")
emacs_workdir=/checkout
emacs_exe=$emacs_workdir/src/emacs

test -x "$emacs_exe"
#                                                     shellcheck disable=SC2154
[[ $CI_PROJECT_DIR ]]
# This script may occasionally be used to build ancillary non-ERC packages,
# but they must still be associated with an ERC bug on the tracker.
[[ ${BUG_NUMBER:=$CI_COMMIT_BRANCH} =~ [[:digit:]]+ ]]
# Non-bug-ticket-based packages must provide an alternative ERC_PACKAGE_NAME
# but still provide a numeric BUG_NUMBER.
: "${ERC_PACKAGE_NAME:=erc-$BUG_NUMBER}"
# Arbitrary serial number to distinguish between variants of a package.
: "${BUG_VERSION:=0}"
# Link to discussion about bug or other relevant info, like source repo.
: "${BUG_URL:="https://debbugs.gnu.org/cgi/bugreport.cgi?bug=$BUG_NUMBER"}"
# Alternate readme.
: "${BUG_README}"

declare -i is_bug
if [[ $ERC_PACKAGE_NAME =~ ^erc-[[:digit:]]+$ ]]; then
    is_bug=1
fi

query_erc_version() {
    prog='(progn (require (quote erc)) (princ erc-version))'
    $emacs_exe -Q -batch -eval "$prog"
}

# Rewrite ERC's published version. If the last component is a number, it's a
# real release, such as "5.5", so append the bug number as a new component.
# Otherwise, it's something like "5.5-git", so extend the existing "-git"
# suffix with the bug number: "5.5-git49860".
make_erc_bug_version() {
    local v
    v=$1
    [[ $v ]]
    if [[ $v =~ [[:digit:]]$ ]]; then
        printf %s "$v.$BUG_NUMBER"
    else
        printf %s "$v$BUG_NUMBER"
    fi
}

# Modify the main ERC library file's Version header and defconst variable.
# This replaces the intervening 0 in $PACKAGE_VERSION with $BUG_VERSION.
patch_lib_main() {
    [[ -e $1 ]]
    local version
    # E.g., 5.6.1snapshot49860.0.20241024.12345 and 5.7alpha0.20241024.12345:
    # [1]="5.6.1snapshot49860" [2]="."     [3]="20241024.12345"
    # [1]="5.7"                [2]="alpha" [3]="20241024.12345"
    [[ $PACKAGE_VERSION =~ ^(.+)(snapshot|alpha|[.])0[.](.+)$ ]]
    if [[ ${BASH_REMATCH[2]} == "." ]]; then
        version=${BASH_REMATCH[1]}.$BUG_VERSION.${BASH_REMATCH[3]}
    else
        version=${BASH_REMATCH[1]}${BASH_REMATCH[2]}$BUG_VERSION.${BASH_REMATCH[3]}
    fi
    # If BUG_VERSION is the default of 0, ensure the above was a waste of time.
    if (( ! BUG_VERSION )); then
        [[ $version == "$PACKAGE_VERSION" ]]
    fi
    sed -i \
        -e "s/;; Version: .*\$/;; Version: $version/" \
        -e "s/^\([(]defconst erc-version \"\).\+\"\$/\1$version\"/" "$1"
    sed -i "s/^@set ERCVER .*\$/@set ERCVER $version/" erc.texi
}

_no_bytecomp_footer='

;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:
'

exempt_tests_from_bytecomp() {
    local tests t
    readarray -t tests < <(find "$1" -name \*.el)
    for t in "${tests[@]}"; do
        # Include a blank line after the variable list.
        echo "$_no_bytecomp_footer" >> "$t"
    done
}

# Note: when updating this, be sure to substitute HTML-reserved chars, like
# "&". See url-insert-entities-in-string.
readme="
WARNING: this is an alpha-quality version of ERC containing preview features
that may be broken and may even ruin your life.
"
if (( is_bug )); then
    readme+="
The first part of the version number is a serial number from the bug tracking
system.
"
fi

modify_erc_elpa_html() {
    # Include everything until and including the first .splice div.
    awk 'NR==1,/class="splice fulldescription/' "$1"
    local label=instructions
    (( is_bug )) && label='bug report'
    cat <<EOF
$readme
Please review the <a href="$BUG_URL">$label</a> before loading the library.
EOF
    # Close the <div class="splice fulldescription"> matched above, since
    # we're keeping it.
    echo '</div>'
    # Include everything in News until the end of the document.
    awk '/<h2>News<.h2>/,/<.body>/' "$1"
    echo '</html>'
}

# Generate a modified elpa-packages file containing a single ERC entry with a
# complicated :shell-command item. It currently
#
#  1. patches the Version header and the defconst "erc-version"
#  2. adds ERC's tests from /test/lisp/erc as a top-level /test directory
#  3. adds a footer to those test to avoid byte compilation
#
_gen_erc_solo_archive_packages() {
    (( $# == 2 ))
    cat <<EOF
(
 ("erc"
  :core ("lisp/erc/"
         "doc/emacs/doclicense.texi"
         "doc/emacs/docstyle.texi"
         "doc/misc/erc.texi"
         "etc/ERC-NEWS"
         "COPYING")
  :excludes ("lisp/erc/erc-loaddefs.el" "lisp/erc/ChangeLog.*")
  :shell-command "
  test -s emacsver.texi && exit 0
  bash $thisfile patch_lib_main erc.el
  bash $thisfile modify_tree
  bash $thisfile exempt_tests_from_bytecomp ./test
  (echo '@set ERCDIST from GNU ELPA'; echo '@set EMACSVER') >emacsver.texi
  "$(insert_additional_archive_package_opts)
  :version-map (("$1" "$2"))
  :doc "erc.texi"
  :news "ERC-NEWS")
)
EOF
}

# Perform additional manipulation of build dir.
modify_tree() {
    cp -vr ../../emacs/test/lisp/erc test
    [[ -r $BUG_README ]] && cp -vf "$BUG_README" .
}

declare -A additional_archive_package_opts

insert_additional_archive_package_opts() {
    if (( ${#additional_archive_package_opts[@]} )); then
        for var in "${!additional_archive_package_opts[@]}"; do
            printf "\n  %s %s" "$var" "${additional_archive_package_opts[$var]}"
        done
    fi
}

gen_erc_solo_archive_packages() {
    # Add a :version-map item like: "5.6.1-git" -> "5.6.1-git49860"
    local version revised
    version=$(query_erc_version)
    revised=$(make_erc_bug_version "$version")
    _gen_erc_solo_archive_packages "$version" "$revised"
}


query_erc_version_and_increment_as_alpha() {
    emacs -Q -batch \
          -eval '(require (quote erc))' \
          -eval '
(let ((v (take 2 (version-to-list erc-version))))
  (princ (format "%s.%s-alpha" (car v) (1+ (cadr v)))))
'
}

# Generate archive-packages contents with one entry for ERC. Give it a
# :version-map item like: "5.6.1-git" -> "5.7-alpha" so that ELPA admin always
# creates an alpha snapshot tarball of a future incremented minor-version.
gen_erc_alpha_archive_packages() {
    if [[ $BUG_README ]]; then
        local basename
        basename=${BUG_README##*/}
        additional_archive_package_opts+=( [:readme]="\"$basename\"" )
    fi
    local version revised
    version=$(query_erc_version)
    revised=$(query_erc_version_and_increment_as_alpha)
    _gen_erc_solo_archive_packages "$version" "$revised"
}

elpa_config='
((sandbox nil) (debug t) (name "ERC Bugs"))
'
# Rewrite the :maintainers item in the archive's top-level pkg-file, e.g.,
# erc-49860-5.6.1snapshot49860.0.20241026.12345/erc-pkg.el, and convert the
# package's name to the version-suffixed name so that it matches the dummy
# stub file generated to stand in for the main library: erc-49860.el.
_modify_erc_pkg_file () {
    [[ -f "$1" ]]
    [[ $1 == */erc-pkg.el ]]
    COMBINE_MAINTS_INPLACE=1 emacs -Q -batch \
        -l "$rootdir/resources/elpa/combine-maints.el" \
        -f "do-pkg-el" "$1"
    sed -i "s/define-package \"erc\"/define-package \"$ERC_PACKAGE_NAME\"/" "$1"
}

modify_erc_pkg_file() {
    (( $# == 1 ))
    [[ -d "$1" ]]
    [[ -f "$1/erc-pkg.el" ]]
    _modify_erc_pkg_file "$1/erc-pkg.el"
}

modify_erc_pkg_file_and_rename() {
    modify_erc_pkg_file "$1"
    mv "$1/erc-pkg.el" "$1/$ERC_PACKAGE_NAME-pkg.el"
}

modify_pkg_file_function=modify_erc_pkg_file

# Perform surgery on the contents of the generated tar file by unarchiving it
# temporarily. The first arg must be the full name of the directory after it's
# unarchived. The second must be that name with the leading package name
# portion suffixed by the bug number.
modify_erc_package_archive () {
    [[ -f $1.tar ]]
    [[ ! -f $1 ]]
    [[ ! -e $2 ]]
    [[ ! -e $2.tar ]]
    tar -xf erc.tar
    # Modify the unarchived erc-pkg.el file.
    $modify_pkg_file_function "$1"
    # Create version-suffixed main library in unarchived package directory.
    awk "/;;; erc.el/,/;;; Code/ {sub(\"erc.el\", \"$ERC_PACKAGE_NAME.el\"); print}" \
        "$1/erc.el" > "$1/$ERC_PACKAGE_NAME.el"
    # The original archive still bears the "erc-" prefix (not version-suffixed).
    # Change that while overwriting the archive.
    mv "$1" "$2"
    # New name should be like: erc-49860-5.6.1snapshot49860.0.20241026.12345.tar.
    tar -cf "$2.tar" "$2"
    # Remove unused files.
    rm -rf "$bdir" "$adir.tar" erc.tar
    # Finally, update the package tarball's a well known location.
    ln -sf "$bdir.tar" "$ERC_PACKAGE_NAME.tar"
}

# Split package web page ($1) and main ELPA landing page (index.html). Remove
# index.html afterwards. Save two files: index.html.top and index.html.bottom.
partition_package_html() {
    [[ $1 == *.html ]]
    # Filter out table lines in index.html containing links to "$1".
    grep -vF "$1" index.html > index.html.template
    # Partition this file using the top-level table element as a sentinel:
    awk 'NR == 1,/<th>Description/' index.html.template > index.html.top
    # Ensure the start of the table exists where expected. Upstream added some
    # ranking stuff in 2024, so we now work around that.
    [[ $(tail -n1 index.html.top) == *Package*Version*Description* ]] 
    awk '/<.table>/,/<.body>/' index.html.template > index.html.bottom
    echo '</html>' >> index.html.bottom
    # Remove all atom-feed related files.
    rm -f index.html index.html.template ./*.xml .aggregated-feed.xml
    # Perform remaining modifications for the package-specific html page.
    # Remove all script elements and the atom feed stuff. Remove links to
    # bogus cgit repo. Restore pre-2024 classes to <header> and <footer>.
    # Close the <div class="wrapper">. Remove jslicense stuff because we have
    # no JavaScript.
    sed -i \
        -e '/<script /d' \
        -e "/Browse ELPA's repository/d" \
        -e 's|<link href="erc.xml.\+/>"||' \
        -e 's|<header class="small">|<header class="header small">|' \
        -e 's| <a class="badge".\+</a>||' \
        -e 's|<footer>|<footer class="footer">|' \
        -e 's|<p><a href="/jslicense.html".\+</p>||' \
        -e 's|</body>|</div></body>|' \
        "$1"
}

# Modify the main ELPA index.html to be more to our liking. In 2024, upstream
# added a <tbody> element right before the first <tr>. We need that on the
# previous line so rows appear on their own. Also, we have no use for
# rankings, so remove that column completely. Restore pre-2024 styling by
# adding .footer class to <footer> and .header class to <header>.
modify_main_index() {
    sed -i \
        -e '/<script /d; /rel="alternate"/d' \
        -e 's|<header class="small">|<header class="header small">|' \
        -e 's|<tbody><tr>|<tr>|' \
        -e 's|<th>Rank</th></tr></thead>|</tr></thead><tbody>|' \
        -e 's|<footer>|<footer class="footer">|' \
        -e 's|<p><a href="/jslicense.html".\+</p>||' \
        index.html
}

# Add the bug number to the package's row in the main index.html table. Each
# row used to be something like:
#
#   <tr><td><a href=foo.html">foo</a></td><td>0.1</td><td>Blah</td></tr>"
#
# But upstream added a "Rank" column in 2024. The placeholder is a "?", which
# we replace with a bug number.
splice_bug_number_in_tr (){
    [[ $1 ]] # package name
    local expr link_text
    expr='^(.+</td><td>).+(</td><td>[?]</td></tr>)$'
    if [[ $BUG_URL == *debbugs* ]]; then
        link_text=bug#$BUG_NUMBER
    else
        link_text=info
    fi
    [[ $(grep "$1.html" index.html) =~ $expr ]]
    {
        printf %s "${BASH_REMATCH[1]}"
        printf %s "<a href=\"$BUG_URL\">$link_text</a>"
        printf %s "</td></tr>"
        echo
    }
}

# Massage web content. Update plain hypertext links in original package web
# page (e.g., erc.html) so they point to a snapshot-specific web page, e.g.,
# erc-49860-5.6.1snapshot49860.0.20241014.12345.html. Instead of updating the
# index itself, do this by partitioning it for later reassembly. Specifically,
# create a erc-49860.index.html.entry stub file containing a table item to
# describe the bug on the main index.html page (once reassembled), and link to
# the renamed snapshot-specifc page. See the other end of this operation in
# /resources/gitlab-ci/pages/merge_archive_contents.sh.
create_erc_bug_snapshot_pages() {
    [[ ! -e $1 ]]
    [[ -e $2.tar ]]
    [[ ! -e $ERC_PACKAGE_NAME.index.html.entry ]]
    modify_main_index
    # These bug snapshots require so much surgery that it's probably simpler
    # to just rewrite the entire row (oh well).
    splice_bug_number_in_tr erc | \
        sed -e "s/erc.html/$2.html/" \
            -e "s|erc</a>|$ERC_PACKAGE_NAME</a>|" \
        > "$ERC_PACKAGE_NAME.index.html.entry"
    # Split index.html into separate pieces for the benefit of the reassembly
    # script, which adds a table element for each tarball between the two.
    partition_package_html erc.html
    # Update package name with version-suffixed one in what will be the
    # snapshot-specific page.
    sed -i \
        -e "s|>erc<|>$ERC_PACKAGE_NAME<|" \
        -e "s|$1.tar|$2.tar|" \
        -e "s|doc/erc.html|doc/$2.html|" \
        -e "s|erc</title>|$ERC_PACKAGE_NAME</title>|" \
        -e "s|erc</h1>|$ERC_PACKAGE_NAME</h1>|" \
        erc.html
    # Replace original page with modified one that includes the README.
    if [[ $BUG_README ]]; then
        mv erc.html "$2.html"
        echo '</html>' >> "$2.html"
    else
        modify_erc_elpa_html erc.html > "$2.html"
        rm -rf  erc-readme.txt erc.html
    fi
    # Save a link under /archive/package-name.html for versioned packages.
    # The GitLab HTTP server resolves /erc-edge to /erc-edge.html.
    [[ -e "$ERC_PACKAGE_NAME.html" ]] ||
        ln -s "$2.html" "$ERC_PACKAGE_NAME.html"
}

gnucss=https://www.gnu.org/software/gnulib/manual.css

replace_doc_stylesheet() {
    [[ -e $1 ]]
    # FIXME when updating pinned ELPA version, check that newer versions of
    # elpa admin don't already do this.
    sed -i '/<[/]head>/i\<link rel="stylesheet" href="'"$gnucss"'">' "$1"
}

# Make package documentation subdir snapshot-specific. Also create a well
# known link for sharing on mailing lists and IRC.
modify_erc_pkg_docs() {
    [[ $1 ]] # bdir
    [[ ! -e $1 ]]
    [[ -e $1.tar ]]
    pushd doc
    mv erc "$1"
    # Update docs to use GNU CSS file.
    replace_doc_stylesheet "$1/erc.html"
    ln -sf "$1/erc.html" "$1.html"
    # Also save this to well known locations. When archives for multiple
    # archives are extracted atop each other by date, the newest survives.
    ln -sf "$1/erc.html" "erc.html"
    [[ -e "$ERC_PACKAGE_NAME.html" ]] ||
        ln -s "$1/erc.html" "$ERC_PACKAGE_NAME.html"
    popd
}

# This snippet extracts the lone (erc ...) entry while preserving indentation.
# It also suffixes the alist's "key" symbol with the bug number, e.g., erc ->
# erc-49860.
gen_isolated_erc_entry () {
    local suffix
    suffix=${ERC_PACKAGE_NAME#erc-*}
    cat <<EOF
(with-temp-buffer (insert-file-contents "archive-contents")
  (goto-char (point-min))
  (down-list) (forward-sexp) (raise-sexp)
  (down-list) (forward-sexp) (insert "-$suffix")
  (message "%s" (buffer-substring (1+ (point-min)) (1- (point-max)))))
EOF
}

# Replace singleton archive-contents catalog with a stub that will later be
# recombined with multiple entries for the same package.
modify_erc_archive_contents() {
    # Merge :maintainers in archive-contents entry.
    COMBINE_MAINTS_INPLACE=1 emacs -Q -batch \
        -l "$rootdir/resources/elpa/combine-maints.el" \
        -f "do-ac" archive-contents
    # Convert archive-contents into stub.
    emacs -Q -batch -eval "$(gen_isolated_erc_entry)" &> archive-contents-entry.eld
    rm -f archive-contents
}

# Perform hacky modifications on the output archive dir: brittle but sadly
# necessary lest we learn how ELPA Admin really works. The main purpose is to
# honor the request of the Emacs developers and ensure a bug-suffixed version
# always shadows the built-in ERC but has a different package name: See
# https://lists.gnu.org/archive/html/bug-gnu-emacs/2022-04/msg01580.html for
# additional context.
#
# For some ERC bug#49860, alter the contents of /archive as follows:
#
#  [archive-contents]              // deteled
#   archive-contents-entry.eld     // stub w. isolated archive-contents entry
#
#   doc/
#  [doc/erc/]                                                // renamed
#   doc/erc-49860-5.6.1snapshot49860.0.20241027.12345/       // ^
#  [doc/erc.html -> doc/erc/erc.html]                        // renamed
#   doc/erc-49860-5.6.1snapshot49860.0.20241027.12345.html   // ^
#   doc/erc.html -> ^                                        // new symlink
#
#  [index.html]                    // deleted
#   index.html.bottom              // orig index.html before the main table
#   index.html.top                 // orig index.html after the main table
#   erc-49860.index.html.entry     // stub w. modified index.html table row
#
#  [erc-5.6.1snapshot49860.0.20241027.12345.html]      // renamed
#   erc-49860-5.6.1snapshot49860.0.20241027.12345.html ^
#  [erc-5.6.1snapshot49860.0.20241027.12345.tar]       // renamed
#   erc-49860-5.6.1snapshot49860.0.20241027.12345.tar  ^
#
#   erc-49860-readme.txt
#   erc-49860.tar -> erc-49860-5.6.1snapshot49860.0.20241027.12345.tar
#   erc.svg
modify_archive_files_for_erc_bug() {
    local adir bdir
    # Create README file to appear in describe-package summary.
    if [[ ! $BUG_README ]]; then
        {
            local label=instructions
            (( is_bug )) && label="bug report"
            printf '\n%s\nPlease read the %s before loading the library.\n\n%s\n' \
                   "$readme" "$label" "$BUG_URL"
        } > "$ERC_PACKAGE_NAME-readme.txt"
    fi
    # Get dereferenced archive name from the symlink's well known name: e.g.,
    # erc.tar -> erc-5.6.1snapshot49860.0.20241026.12345.
    adir=$(readlink erc.tar) adir=${adir%*.tar}
    # Use that to generate the new full version-suffixed base name, e.g.,
    # erc-49860-5.6.1snapshot49860.0.20241026.12345.
    bdir=$ERC_PACKAGE_NAME-${adir#*erc-}
    modify_erc_package_archive "$adir" "$bdir"
    # Replace archive-contents with a stub for just one entry.
    modify_erc_archive_contents
    # Create HTML stub files.
    create_erc_bug_snapshot_pages "$adir" "$bdir"
    # Update documentation to be bug-specific.
    modify_erc_pkg_docs "$bdir"
}

elpa_sha=fc29988d84e27414dc5bccc3585acb506079631d # Oct 23 2024
elpaa_sha=748963ce889f111a55b42c97b515fd72b097130d # Sep 25 2024

gen_archive_package_function=gen_erc_solo_archive_packages

make_erc_bug() {
    pushd elpa
    git checkout $elpa_sha
    # For now, require a fresh repo.
    [[ ! -e emacs ]]
    [[ ! -e GNUmakefile ]]
    [[ ! -e packages/erc ]]
    printf %s "$elpa_config" > elpa-config
    $gen_archive_package_function > elpa-packages
    git clone $emacs_workdir emacs
    make setup
    git -C admin checkout $elpaa_sha
    make packages/erc
    # Run elpaa-batch-pkg-spec-make-dependencies, which writes
    # "packages/erc/erc-pkg.el: packages/erc/erc.el\n" to elpa/.pkg-descs.mk
    # before running elpaa-batch-make-one-package.
    make build/erc
    make archive-devel/index.html
    popd
    mv elpa/archive-devel archive
    pushd archive
    modify_archive_files_for_erc_bug
    popd
}

# Create an ERC variant that's a combination of patches from multiple bugs.
make_erc_alpha() {
    # Package has a name like "erc-edge".
    (( ! is_bug ))
    # URL has been shadowed with something non-bug related.
    [[ $BUG_URL != *debbugs* ]]
    # Use a future alpha version instead of a bug-numbered snapshot.
    gen_archive_package_function=gen_erc_alpha_archive_packages
    # Unlike with snapshots, package--description-file returns erc-X-pkg.el
    # rather than erc-pkg.el if the input contains "alpha". We must therefore
    # create the erc-X-pkg.el file manually so users can install the tarball
    # with `package-install-file'.
    modify_pkg_file_function=modify_erc_pkg_file_and_rename
    make_erc_bug
}

_solo_package_ac_entry='
(with-temp-buffer (insert-file-contents "archive-contents")
  (goto-char (point-min))
  (down-list) (forward-sexp) (raise-sexp)
  (message "%s" (buffer-substring (1+ (point-min)) (1- (point-max)))))'


# For some package 'foo', alter the members of /archive as follows:
#
#  [archive-contents]              // deleted
#   archive-contents-entry.eld     // stub w. isolated archive-contents entry
#
#   doc/                           // likely empty
#
#  [index.html]                    // deleted
#   index.html.bottom              // orig index.html before the main table
#   index.html.top                 // orig index.html after the main table
#   foo.index.html.entry           // stub w. modified index.html table row
#
#   foo-0.1snapshot0.20241022.12345.tar
#   foo-readme.txt
#   foo.html
#   foo.svg
#   foo.tar -> foo-0.1snapshot0.20241022.12345.tar
modify_archive_files_for_solo_package() {
    [[ $1 ]] # package name
    # The project README will have been copied to $1.readme.txt.
    emacs -Q -batch -eval "$_solo_package_ac_entry" &> archive-contents-entry.eld
    rm -f archive-contents
    # Create stub table row for the reassembled index.html. Add the bug number
    # with a link to the bug report.
    modify_main_index
    splice_bug_number_in_tr "$1" > "$1.index.html.entry"
    partition_package_html "$1.html"
    if [[ -f doc/$1.html ]]; then
        replace_doc_stylesheet "doc/$1.html"
    fi
}

# Build a package other than a variant of ERC itself. The name argument is
# different from ERC_PACKAGE_NAME, which is only used for building ERC
# variants.
make_solo_package() {
    [[ $1 ]] # name of the package
    [[ $2 ]] # URL to fetch the package from
    pushd elpa
    git checkout $elpa_sha
    # Require a fresh ELPA repo.
    [[ ! -e GNUmakefile ]]
    [[ ! -e packages/$1 ]]
    printf %s "$elpa_config" > elpa-config
    echo "((\"$1\" :url \"$2\"))" > elpa-packages
    make setup
    git -C admin checkout $elpaa_sha
    git clone "$2" "packages/$1"
    make "build/$1"
    if [[ -d archive-devel ]]; then
        make archive-devel/index.html
    else
        make archive/index.html
    fi
    popd
    if [[ -d elpa/archive-devel ]]; then
        mv elpa/archive-devel archive
    else
        mv elpa/archive archive
    fi
    pushd archive
    modify_archive_files_for_solo_package "$1"
    popd
}

"$@"
