;;; hit-irker.el --- Emit msg to IRC bot webhook  -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Actual API is custom (unrelated to irker).
;;

;;; Code:
(require 'url-http)
(require 'mm-url)

(defvar hit-irker-fmt "%s \u2771\u2771 \002%s\002 %s [%s] %s \u25cf %s")

(defun hit-irker--request (target)
  "TARGET."
  (if-let ((service-url (getenv "HI_SERVICE"))
           (head-summary (getenv "HI_SUMMARY"))
           (secret (getenv "HI_SECRET"))
           (short-sha (getenv "CI_COMMIT_SHORT_SHA"))
           (branch (getenv "CI_COMMIT_BRANCH"))
           (desc (getenv "CI_COMMIT_TITLE")))
      (let* (done
             (network-security-level 'low)
             (cb (lambda (&rest _r)
                   (save-restriction
                     (widen)
                     (goto-char (point-min))
                     (unless (search-forward-regexp "HTTP.*200" nil t)
                       (error "Request failed: %S" (buffer-string)))
                     (setq done t))))
             (url-request-extra-headers
              `(("Authorization" . ,(concat "Bearer " secret))
                ("Content-Type" . "application/x-www-form-urlencoded")))
             (msg (format hit-irker-fmt
                          target branch emacs-version
                          short-sha
                          desc (string-trim-right head-summary)))
             (url-request-method "POST")
             (url (url-generic-parse-url service-url))
             url-request-data)
        (when (> (length msg) 360)
          (setq msg (concat (substring url-request-data 0 356) "...")))
        ;; Send data as form key with no value and no = delim
        (setq url-request-data (mm-url-form-encode-xwfu msg))
        (with-current-buffer (if (string= (url-type url) "https")
                                 (url-https url cb '(nil))
                               (url-http url cb '(nil)))
          (with-timeout (3 (message "Response: %s" (buffer-string))
                           (error "Failed"))
            (while (not done)
              (sleep-for 0.1)))))
    (error "Missing required env vars")))

(defun hit-irker-main ()
  "Send update to #chan or @nick given on command line."
  (let ((target (car command-line-args-left)))
    (hit-irker--request target)))

(provide 'hit-irker)

;;; hit-irker.el ends here
