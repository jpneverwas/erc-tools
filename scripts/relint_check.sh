#!/bin/sh

set -e
cd /checkout

src/emacs -batch \
    -eval '(package-refresh-contents)' \
    -eval '(package-install (quote relint))'

src/emacs -batch -f package-initialize \
    -l relint -f relint-batch lisp/erc test/lisp/erc
